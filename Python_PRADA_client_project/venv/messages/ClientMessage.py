from __future__ import annotations
from abc import *


class ClientMessage(ABC):

    @abstractmethod
    def to_bytes(self) -> bytearray:
        pass
