from messages.ClientMessage import ClientMessage

class RawMessage( ClientMessage ):
    def __init__(self, bytes):
        self.byterep = bytes


    def to_bytes(self) -> bytearray:
        return self.byterep
