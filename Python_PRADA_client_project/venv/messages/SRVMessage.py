import copy


class SRVMessage:

    def __init__(self, message_id, protocol_fields: bytearray, payload: bytearray):
        self.message_id = message_id
        self.protocol_fields = copy.deepcopy(protocol_fields)
        self.payload = copy.deepcopy(payload)
        #  print("{:08b}".format(int(self.byte_representation.hex(), 16)))