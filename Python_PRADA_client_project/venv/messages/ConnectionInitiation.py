from messages.ClientMessage import ClientMessage


class ConnectionInitiation(ClientMessage):



    def __init__(self, want_ui: bool, req_size: bool, protocol_vers: int, req_rows=255, req_columns=255):
        self.byte_representation = bytearray()
        self.message_id = 0
        self.byte_representation.append(self.message_id)
        self.byte_representation.append(0)  # sequence number
        self.byte_representation.append(0)

        if want_ui:
            self.byte_representation[0] |= 8

        if req_size:
            self.byte_representation[0] |= 4
            self.byte_representation.append(req_rows)
            self.byte_representation.append(req_columns)


        self.byte_representation[0] |= protocol_vers
        #  print("{:08b}".format(int(self.byte_representation.hex(), 16)))

    def to_bytes(self):
        return self.byte_representation
