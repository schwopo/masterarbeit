from messages.ClientMessage import ClientMessage


class LinkClicked(ClientMessage):



    def __init__(self,link_id, has_ack: bool, seq_no, ack_no = 0):
        self.byte_representation = bytearray()
        self.message_id = 1
        self.byte_representation.append(self.message_id << 4)
        if(has_ack):
            self.byte_representation[0] |= 8

        seq_no_bytes = seq_no.to_bytes(2, byteorder='big')
        self.byte_representation.append(seq_no_bytes[0])  # sequence number
        self.byte_representation.append(seq_no_bytes[1])

        if (has_ack):
            ack_no_bytes = ack_no.to_bytes(2, byteorder='big')
            self.byte_representation.append(ack_no_bytes[0])  # ack no
            self.byte_representation.append(ack_no_bytes[1])

        self.byte_representation.append(link_id)

       # print("{:08b}".format(int(self.byte_representation.hex(), 16)))

    def to_bytes(self) -> bytearray:
        return self.byte_representation