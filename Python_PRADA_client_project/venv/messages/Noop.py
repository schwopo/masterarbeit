from messages.ClientMessage import ClientMessage


class Noop(ClientMessage):

    def __init__(self, ack_no = 0):
        self.byte_representation = bytearray()
        self.message_id = 10
        self.byte_representation.append(168)

        ack_no_bytes = ack_no.to_bytes(2, byteorder='big')
        self.byte_representation.append(ack_no_bytes[0])  # ack no
        self.byte_representation.append(ack_no_bytes[1])

        #print("{:08b}".format(int(self.byte_representation.hex(), 16)))

    def to_bytes(self) -> bytearray:
        return self.byte_representation