from utils.ByteUtils import *

from windowcontents.Content import Content
from windowcontents.TextContent import TextContent
from windowcontents.WindowTag import WindowTag
from windowcontents.SplitTag import SplitTag
from windowcontents.EndWinTag import EndWinTag
from windowcontents.JumpToTag import JumpToTag
from windowcontents.JumpForTag import JumpForTag
from windowcontents.LinkTag import LinkTag
from windowcontents.DataField import ConstantDataField
from windowcontents.InputFieldTag import InputField

from protocolDataTypes.PDT import DataType

from protocolDataTypes.UFixed32 import UFixed32
from protocolDataTypes.SFixed32 import SFixed32
from protocolDataTypes.Float import Float

from protocolDataTypes.UFixed64 import UFixed64
from protocolDataTypes.SFixed64 import SFixed64
from protocolDataTypes.Double import Double

from protocolDataTypes.Boolean import Boolean
from protocolDataTypes.Uint32 import Uint32
from protocolDataTypes.Uint64 import Uint64
from protocolDataTypes.Sint32 import Sint32
from protocolDataTypes.Sint64 import Sint64

from protocolDataTypes.String import String
from protocolDataTypes.Bytes import Bytes


def decode_ui_update(UIdata: bytearray):

    #print(getBinString(UIdata))
    update = []
    skip_next_n_iterations = 0

    for index, byte in enumerate(UIdata):
        if skip_next_n_iterations > 0:
            skip_next_n_iterations -= 1
        else:
            # Reads all texts encoded in utf_8 that is concatinated:
            if byte >> 7 == 0 or byte >> 5 == 6 or byte >> 4 == 14 or byte >> 3 == 30:
                itr = 0
                utf8_text = bytearray()
                while True:
                    #  append first byte
                    #print("First: " +getBinString([UIdata[index + itr]]) +" "+ getHexString([UIdata[index + itr]]))
                    first_UTF8_byte = UIdata[index + itr]
                    utf8_text.append(first_UTF8_byte)

                    # how many more bytes belong to this utf-8 char?

                    following_n_utf_8_bytes = 0
                    if(not first_UTF8_byte >> 7 == 0):
                        offset = 6  # 6 because #1 gives length of whole utf-8 char, but first one is already appended
                        # print(self.getHexString([first_UTF8_byte]))

                        while (first_UTF8_byte >> offset) % 2 == 1:
                            following_n_utf_8_bytes += 1
                            offset -= 1

                    #  append them all
                    # print(following_n_utf_8_bytes)
                    while following_n_utf_8_bytes > 0:
                        itr += 1
                        #print("Follow Appending: " + " " + getBinString([UIdata[index + itr]]) +" "+ getHexString([UIdata[index + itr]]))
                        utf8_text.append(UIdata[index + itr])
                        following_n_utf_8_bytes -= 1

                    skip_next_n_iterations = itr

                    itr += 1  # index of next byte to read. May be text may be not
                    if (index + itr >= len(UIdata)):
                        break
                    else:
                        nextByte = UIdata[index + itr]
                        if not (nextByte >> 7 == 0 or nextByte >> 5 == 6 or nextByte >> 4 == 14 or nextByte >> 3 == 30):
                            break

                #print(utf8_text.decode('utf-8'))
                update.append(TextContent(utf8_text))

            # Inline tags
            elif (byte >> 6 == 2):
                contentType = (byte & 63) >> 3

                # windowTag
                if contentType == 0:
                    keep_content = (byte & 7) >> 2
                    update.append(WindowTag(keep_content, UIdata[index + 1]))
                    skip_next_n_iterations = 1

                # SplitTag
                elif contentType == 1:
                    split_dir = (byte & 7) >> 2
                    update.append(SplitTag(split_dir, UIdata[index + 1], UIdata[index + 2], UIdata[index + 3]))
                    skip_next_n_iterations = 3

                #  JumpForTag
                elif contentType == 2:
                    whitespace = (byte & 4) >> 2
                    fields = (byte & 3)
                    skip_next_n_iterations = 1
                    if (fields == 1):
                        update.append(JumpForTag(whitespace, False, True, None, UIdata[index + 1]))
                    elif (fields == 2):
                        update.append(JumpForTag(whitespace, True, False, UIdata[index + 1], None))
                    elif (fields == 3):
                        update.append(JumpForTag(whitespace, True, True, UIdata[index + 1], UIdata[index + 2]))
                        skip_next_n_iterations = 2

                #  JumpToTag
                elif contentType == 3:
                    update.append(JumpToTag(UIdata[index + 1], UIdata[index + 2]))
                    skip_next_n_iterations = 2

                #  LinkTag
                elif contentType == 4:
                    #  Read Link ID
                    link_id = UIdata[index + 1]

                    # Determine data Type of the display value
                    data_type = None
                    skip_next_n_iterations = 1
                    if (byte & 4) >> 2 == 1:
                        data_type = DataType.STRING
                        data_type_index = index + 2
                    else:
                        data_type = decode_data_type(UIdata[index + 2])
                        data_type_index = index + 3
                        skip_next_n_iterations += 1

                    pdt, pdt_byte_size = extract_pdt(data_type, UIdata, data_type_index)
                    skip_next_n_iterations += pdt_byte_size

                    update.append(LinkTag(link_id, pdt))

                #Data Field
                elif contentType == 5:
                    if byte & 6 == 0:      #const. data field
                        tag_index = 1

                        has_fw = byte & 1
                        if has_fw:
                            field_width = UIdata[index + tag_index]
                            print("Field Width: " + str(field_width))
                            tag_index += 1

                        data_type_id = decode_data_type(UIdata[index + tag_index])
                        skip_next_n_iterations += tag_index
                        tag_index += 1

                        pdt, pdt_byte_size = extract_pdt(data_type_id, UIdata, index + tag_index)
                        skip_next_n_iterations += pdt_byte_size

                        if(has_fw):
                            update.append(ConstantDataField(has_fw, pdt, field_width))
                        else:
                            update.append(ConstantDataField(has_fw, pdt))

                #  Input Field
                elif contentType == 6:

                    end_of_line = (byte & 7) >> 2
                    dfault_text = (byte & 3) >> 1
                    hide_text   = (byte & 1)
                    #print(str(end_of_line) + " " + str(dfault_text) + " "+ str(hide_text))
                    input_field_id = UIdata[index + 1]
                    #print(input_field_id)

                    tag_index = index + 2 # field_width / data type
                    field_width = None

                    if(not end_of_line):
                        field_width = UIdata[tag_index]
                        tag_index += 1

                    pdt_id = decode_data_type(UIdata[tag_index])
                    tag_index += 1 # Begin of varint -> max-inp_size

                    max_inp_size_varint, varint_size = extract_varint(UIdata, tag_index)
                    max_inp_size = int_from_varint(max_inp_size_varint)
                    tag_index += varint_size

                    pdt_string = None
                    if(dfault_text):
                        pdt_string, pdt_string_size = extract_pdt(DataType.STRING, UIdata, tag_index)
                        tag_index += pdt_string_size

                    skip_next_n_iterations = tag_index - index - 1

                    update.append(InputField( end_of_line, dfault_text, hide_text, input_field_id , pdt_id,  max_inp_size, field_width, pdt_string))
                # FrameEnd: all content for window is given append and set new window_id
                elif contentType == 7:
                    update.append(EndWinTag(byte & 7))

            else:
                print("This is no valid window content!")

    return update

#  return the amount of bytes to skip over.
def extract_pdt(data_type, bytes: bytearray, index):
    bytes_to_skip = 0
    pdt = None
    value = bytearray()
    #  Fixed Size 32 bit pdt:
    if data_type <= DataType.FLOAT:
        value = bytes[index : index + 4]
        bytes_to_skip = 4  #  4 byte value
        if(data_type == DataType.UFIXED32):
            pdt = UFixed32(value)
        elif (data_type == DataType.SFIXED32):
            pdt = SFixed32(value)
        elif (data_type == DataType.FLOAT):
            pdt = Float(value)

    elif (data_type <= DataType.DOUBLE):
        value = bytes[index : index + 8]
        bytes_to_skip =  8  #  8 byte value
        if (data_type == DataType.UFIXED64):
            pdt = UFixed64(value)
        elif (data_type == DataType.SFIXED64):
            pdt = SFixed64(value)
        elif (data_type == DataType.Double):
            pdt = Double(value)
    else: # Now comes a varint. Maybe a value or a size of a following value
        varint,varint_size = extract_varint(bytes, index)  # Get varint out of next x bytes
        bytes_to_skip += varint_size
        index += varint_size

        if(data_type <= DataType.SINT64):
            if (data_type == DataType.BOOLEAN):
                pdt = Boolean(varint)
            elif (data_type == DataType.UINT32):
                pdt = Uint32(varint)
            elif (data_type == DataType.UINT64):
                pdt = Uint64(varint)
            elif (data_type == DataType.SINT32):
                pdt = Sint32(varint)
            elif (data_type == DataType.SINT64):
                pdt = Sint64(varint)
        else:# Length delimited data type. Read varint contains data size of string or byte
            data_size = int_from_varint(varint)
            bytes_to_skip += data_size
            # Read delimited value
            if(data_type == DataType.STRING):
                pdt = String(bytes[index : index + data_size])  #  [include:exclude]
            elif(data_type == DataType.BYTES):
                pdt = Bytes(bytes[index : index + data_size])


    return pdt, bytes_to_skip

def decode_data_type(byte):
    encoding_type = byte >> 6
    offset = 0                  #   Length fixed 4 byte / 32 bit
    if encoding_type == 1:      #   Length fixed 8 byte / 64 bit
        offset = 1000
    elif(encoding_type == 2):   #   Varint types
        offset = 2000
    elif(encoding_type == 3):   #   Length-delimited
        offset = 3000
    data_type_id = byte & 63
    print("ENCODING ID: " + str(offset + data_type_id))
    return offset + data_type_id

def extract_varint(bytes: bytearray, index):
    varint = bytearray()  # Get varint out of next x bytes
    varint_size = 0
    while True:
        current_byte = bytes[index + varint_size]
        varint.append(current_byte)
        varint_size += 1

        if (not current_byte >> 7 == 1):
            return varint,  varint_size

