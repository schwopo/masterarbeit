def int_from_varint(varint):
    value = 0
    itr = 0
    for byte in varint:
        value |= (byte & 127) << (7 * itr)
        itr += 1
    return value

def getBinString(byteArray:bytearray):
    binString = ""
    for byte in byteArray:
        binString += (format(byte, '08b') + ' ')
    return binString[:-1]

def getHexString(byteArray:bytearray):
    hexString = ""
    for byte in byteArray:
        hexString += ("%0.2X" % byte) + " "
    return hexString

def decode_zigzag_32bit(encoded_number):
    '''
    if encoded_number & 1:
        return -((encoded_number + 1) / 2)
    else:
        return (encoded_number / 2)
    '''
    return ((encoded_number % 0x100000000) >> 1) ^ (-(encoded_number & 1))

def decode_zigzag_64bit(encoded_number):
    '''
    if encoded_number & 1:
        return -((encoded_number + 1) / 2)
    else:
        return (encoded_number / 2)
    '''
    return ((encoded_number % 0x10000000000000000) >> 1) ^ (-(encoded_number & 1))


def rshift_32bit(val, n): return (val % 0x100000000) >> n

"Encoding "