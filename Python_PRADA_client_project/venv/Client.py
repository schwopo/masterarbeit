from Model import Model
from View import View
from Controller import Controller

model = Model()
view = View(model)
c = Controller(model,view)