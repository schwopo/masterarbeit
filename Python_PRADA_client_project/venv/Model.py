from typing import List
import copy

from BinTree import TreeNode
from utils.UIUpdateUtils import decode_ui_update

from windowcontents.Content import Content


class Model():

    def __init__(self):
        print("Model initialised")
        self.binWinTree = TreeNode(0)

    def updateUIStructure(self, UIdata:bytearray):
        #print(getHexString(UIdata))
        update = decode_ui_update(UIdata)

        #Save content of input fields and delete them later if overwritten
        inp_field_dic = self.binWinTree._observers[0].inp_field_dic

        inp_field_before_content_dic = {}
        for key in inp_field_dic:
            value = inp_field_dic[key]
            input_field = value[0]
            inp_field_before_content_dic[key]= input_field.gather()

        current_window_id = 0
        window_update: [Content] = []

        keep_content = False
        for item in update:
            #  WindowTag
            if item.content_id == Content.WINDOW:
                if(len(window_update) > 0):
                    self.binWinTree.changeContent(current_window_id, copy.deepcopy(window_update), keep_content)
                    #print("window " + str(current_window_id) + " has ended")
                    window_update.clear()
                else:   #  If there is currently no content. We are just switching between windows.
                    pass

                current_window_id = item.win_id
                keep_content = item.keep_content #This tag decided if already established content will be kept

            #  SplitTag
            elif item.content_id == Content.SPLIT:
                #print("split: " + str(current_window_id) + " -> " + str(item.fstWinID) + " "+ str(item.sndWinID))
                self.binWinTree.split(current_window_id, item.splitDir, item.splitMargin,
                                      TreeNode(item.fstWinID), TreeNode(item.sndWinID))
                current_window_id = item.fstWinID

            #  End of window content
            elif item.content_id == Content.END_WIN:
                #print("window " + str(current_window_id) +" has ended")
                self.binWinTree.changeContent(current_window_id, copy.deepcopy(window_update), keep_content)
                window_update.clear()
                skips = item.skips + 1
                #print ("CN: " + str(current_window_id) +" skips: " + str(skips))

                while(skips> 0):
                    current_window_id = self.binWinTree.nextNodeLeftBound(current_window_id)
                    #print("Next Node: " + str(current_window_id))
                    skips -= 1

                keep_content = False #Always the case if we jump with endwin
            #  Text
            elif item.content_id == Content.TEXT or item.content_id == Content.JUMP_TO or item.content_id == Content.JUMP_FOR or \
                    item.content_id == Content.LINK or item.content_id == Content.CONSTANT_DF or item.content_id == Content.INPUT_FIELD:
                window_update.append(item)

        self.binWinTree.changeContent(current_window_id, copy.deepcopy(window_update), keep_content)

        for key in inp_field_before_content_dic:
            value = inp_field_dic[key]
            input_field = value[0]
            if not inp_field_before_content_dic[key] == input_field.gather():
               del input_field
               inp_field_dic.pop(key)