from queue import SimpleQueue
from threading import Thread
from ecdsa import ECDH, SECP256k1
import time
import hashlib
import random
import otway_rees

from Connection import Connection

from messages.ClientMessage import ClientMessage
from messages.ConnectionInitiation import ConnectionInitiation
from messages.LinkClicked import LinkClicked
from messages.ClientInput import ClientInput
from messages.Termination import TerminationMSG
from messages.Noop import Noop
from messages.SRVMessage import SRVMessage
from messages.RawMessage import RawMessage

class ConnectionManager:

    def __init__(self):
        self.con = Connection('COM4', 9600)
        self.portReady = self.con.portOpened

        self.connected = False
        self.want_to_term = False

        #Sequencing
        self.client_seq_no = 0
        self.expected_srv_seq_no = 0

        self.msg_send_time = 0       #  Time the last message with data was send(own or reply)
        self.last_own_message = None #  A msg send by the client on its own not a reply.
        self.last_reply = None       #  Last reply to the srv. can be data or just Noop ACK
        self.need_ACK = False

        self.max_sending_tries = 4
        self.sending_tries = 0

        #defaul values,  will be overwritten by first srv msg
        self.msg_timeout    = 5
        self.term_timeout   = 4
        self.srv_timeout    = 30

        #Connection Timeout
        self.last_srv_input_time = 0
        self.connection_closed = False
        self.con_term_time = 0

        #Inform Controller
        self.gotInput = False
        self.lastInput = None
        self.readLastInput = True

        #Otway Rees data
        self.or_own_id = 1
        self.or_server_id = 2

        self.inputQueue = SimpleQueue()
        self.inputThread = Thread(target=self.con.inputLoop, args=(self.inputQueue,))
        self.inputThread.setDaemon(True)

        time.sleep(1)

    def start_input_loop(self):
        if self.portReady == True:
            self.inputThread.start()
            print("Threads and queues initialized")

    def send_or_connection_request(self, ra):
        print("Sending OR Connection Request")
        self.send(otway_rees.generate_connection_request(self.or_own_id, self.or_server_id, ra, self.con.or_key))
        print("Sent OR Connection Request")

    def perform_ecdhe_key_exchange(self):
        ecdh = ECDH(curve=SECP256k1)
        ecdh.generate_private_key()
        local_public_key = ecdh.get_public_key()

        remote_public_key = self.con.receive_gucci_msg()
        ecdh.load_received_public_key_bytes(remote_public_key)

        self.send(RawMessage(local_public_key.to_string()))
        secret = ecdh.generate_sharedsecret_bytes()
        print("ecdhe secret: \n\t", secret.hex())
        hashobj = hashlib.sha256()
        hashobj.update(secret)
        hashbytes = hashobj.digest()
        self.con.enable_cipher(int.from_bytes(hashbytes, byteorder="big"))


    def checkForSRVInput(self):
        if(self.con.recInProgress):
            #Make sure to evaluate the latest input from SRV, before resending a message
            self.readLastInput = False
            self.last_srv_input_time = time.time()

        if(self.connected and (not self.want_to_term) and time.time() - self.last_srv_input_time> self.srv_timeout):
            print("Not heard from SRV in a long time! It timed out")
            self.send_con_term()

        if (self.want_to_term and time.time() - self.con_term_time > self.term_timeout):
            self.connection_closed = True
            print("Term request timed out, reset client!")

        if self.inputQueue.qsize() > 0:
            input:SRVMessage = self.inputQueue.get()
            if not input == None:
                if(input.message_id == 10 and self.need_ACK and self.get_ACK_No(input) == self.client_seq_no + 1):
                    if (self.want_to_term): #  Got ACK for my con termination request
                        self.connection_closed = True
                        print("SRV acked CON_TERM")
                    else:
                        self.got_ACK()
                else:
                    message_seq_no = int.from_bytes(input.protocol_fields[1:3], byteorder='big')
                    #  New input from SRV may be reply or push
                    if(message_seq_no == self.expected_srv_seq_no):
                        # Check for ACK of last our last data msg if we need it
                        # If we need ACK for a reply and we get msg with right seq no -> SRV saw our last msg.
                        # If the ACK got lost we can still deduce that this SRV saw it.
                        if(self.need_ACK and self.last_reply is not None and not self.last_reply.message_id == 10):
                            self.got_ACK()

                        elif(self.need_ACK):
                            next_expected_seq_no = self.get_ACK_No(input)
                            if (next_expected_seq_no == self.client_seq_no + 1):
                                    self.got_ACK()

                        if input.message_id == 0:
                            if (not self.connected):
                                self.connected = True
                                print("We are connected boys!")
                            else:
                                print("Double connection ack?")

                        self.expected_srv_seq_no += 1
                        self.readLastInput = True
                        self.gotInput = True
                        self.lastInput = input
                    elif message_seq_no == self.expected_srv_seq_no - 1:
                        #  If SRV has unsuccessfully tried to reach me, it may try to term con. with last seq.no
                        #  It can not know if i have rec. last srv msg and all acks got lost or all srv msg got lost
                        if(input.message_id == 12):
                            self.readLastInput = True
                            self.gotInput = True
                            self.lastInput = input
                        else:
                            self.resend_last_reply()
                    else:
                        pass  #  not wanted seq no discard input

        elif(self.need_ACK and self.readLastInput):
            if(time.time() - self.msg_send_time >= self.msg_timeout and  (not self.want_to_term)): # response timeout
                if(self.sending_tries >= self.max_sending_tries):
                    if(not self.connected):
                        print("Too many init tries, srv not responding! -> reset srv")
                        self.to_default()
                    else:
                        print("Too many init tries, srv not responding! -> term con")
                        self.send_con_term()

                else:  #Try again
                    self.resend_last_data_msg()

    def got_ACK(self):
        print("Got ACK for last Message")
        self.client_seq_no += 1
        self.sending_tries = 0
        self.last_own_message = None
        self.last_reply = None
        self.need_ACK = False

    def get_ACK_No(self,input:SRVMessage):
        next_expected_seq_no = None
        if(not input.message_id == 11): #  Not a Heartbeat:
            if (input.message_id == 0):
                next_expected_seq_no = int.from_bytes(input.protocol_fields[3:5], byteorder='big')
            elif (input.message_id == 10): #  MSG is Noop with ACK. Should not happen from SRV side right now, but for future scenarios maybe.
                next_expected_seq_no = int.from_bytes(input.protocol_fields[1:3], byteorder='big')
            else:
                firstField = input.protocol_fields[0]
                if( ((firstField & 15) >> 3) == 1 ):
                    next_expected_seq_no = int.from_bytes(input.protocol_fields[3:5], byteorder='big')
        print("Next expected seq no: " + str(next_expected_seq_no))
        return next_expected_seq_no

    def send(self, msg: ClientMessage):
        self.outputThread = Thread(target=self.con.outputLoop, args=(msg,))
        self.outputThread.setDaemon(True)
        self.outputThread.start()

    def send_own_message(self, msg: ClientMessage):
        if(not self.need_ACK):
            print("First try own message!")
            self.send(msg)
            self.msg_send_time = time.time()
            self.last_own_message = msg
            self.need_ACK = True
            self.sending_tries = 1

    def send_reply(self, msg: ClientMessage):
        if not self.need_ACK: #  We can just send reply since we don't need ACK right now
            self.send(msg)
            self.last_reply = msg
            if not msg.message_id == 10: #  Reply is not just Noop ACK, we need ACK ourself, should not happen atm
                self.need_ACK = True
                self.msg_send_time = time.time()
                self.sending_tries = 1
                print("First try data reply")
            else:
                print("Sending ACK")
        else:   # We do need ACK for our own last message. This reply has to wait if it has data.
                # Our last msg can not have been a reply with data. A reply means the SRV had to wait itself for its answer.
                # So if we get data from the srv it means our last reply was received and ACKed with the last data message
            if msg.message_id == 10:
                self.send(msg)
                print("Need ACK but can send ACK")
            else:
                #TODO Put reply into queue, we send it after we have our ACK
                print("Reply has to wait for until my ACK")

    def resend_last_data_msg(self): #Triggered by ACK Timeout, not used when we get old srv msg
        # last_own_msg, last_reply can not both be 0 if i still need ACK. On this SRV it sld for now always be last_own_message
        if(self.last_own_message is not None):
            self.send(self.last_own_message)
        elif(self.last_reply is not None):
            self.send(self.last_reply)

        self.msg_send_time = time.time()
        self.sending_tries += 1

    #  Triggered when incoming seq_no is the last one and not the expected one
    def resend_last_reply(self):
        #  print("Resend")
        if (self.last_reply is not None):
            self.send(self.last_reply)

            #  If last reply is not Noop and the SRV needs it again, our reply was not received and we still need ACK for it
            #  Reset the sending count, because SRV is there.
            if(self.last_reply.message_id != 10):
                self.msg_send_time = time.time()
                self.sending_tries = 1
                print("retry sending response")
            else:
                print("Ack got lost, resend")

    def connect( self, protocol_version, want_ui, req_size = False, req_rows = None, req_columns = None ):
        ra = random.randbytes(otway_rees.nonce_size)
        self.send_or_connection_request(ra)
        ks = self.con.receive_or_session_key(ra)
        self.con.enable_cipher(ks)

        self.perform_ecdhe_key_exchange()

        self.start_input_loop()

        if(not self.need_ACK and (not self.connected)):
            print("Trying to connect to SRV!")
            message = ConnectionInitiation(want_ui, req_size, protocol_version, req_rows, req_columns)
            self.send_own_message(message)
        else:
            print("Already trying to connect or waiting for another response")

    def send_link_clicked( self, link_id ):
        if not self.need_ACK:
            self.send_own_message(LinkClicked(link_id, False, self.client_seq_no))

    def send_user_input( self, input_field_id, pdt : bytearray ):
        if not self.need_ACK:
            self.send_own_message(ClientInput(input_field_id, pdt, False, self.client_seq_no))

    def send_con_term(self):
            print("Terminating connection!")
            self.need_ACK = False
            self.send_own_message(TerminationMSG(False, self.client_seq_no))
            self.con_term_time = time.time()
            self.want_to_term = True

    def send_noop_ack(self):
        self.send_reply(Noop(self.expected_srv_seq_no))
        self.lastInput = None

    def to_default(self):
        print("Reseting Client")
        self.connected = False
        self.want_to_term = False
        # Sequencing
        self.client_seq_no = 0
        self.expected_srv_seq_no = 0

        self.msg_send_time = 0  # Time the last message with data was send(own or reply)
        self.last_own_message = None  # A msg send by the client on its own not a reply.
        self.last_reply = None  # Last reply to the srv. can be data or just Noop ACK
        self.need_ACK = False

        self.sending_tries = 0

        # Connection Timeout
        self.last_srv_input_time = 0
        self.con_term_time = 0
        self.connection_closed = False




