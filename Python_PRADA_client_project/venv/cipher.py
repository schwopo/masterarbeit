from speck import SpeckCipher
from textwrap import wrap

class Cipher:
	block_size = 128
	key_size = 256
	def __init__(self, key):
		#changing key later does not work
		self.speck = SpeckCipher(key, key_size=256, block_size=Cipher.block_size)

	def intbullshittery(self, msg):
		intmsg = int.from_bytes(msg, byteorder="big", signed="false")
		return intmsg

	def chunk(self, data, size):
		return [data[i:i+size] for i in range(0, len(data), size)]


	def unintbullshittery(self, msg):
		return bytearray.fromhex('{:032x}'.format(msg))

	def seperate_blocks(self, msg):
		return self.chunk(msg, self.speck.block_size//8)

	def perform_on_blocks(self, msg, fn):
		msg = bytes(msg)
		blocks = self.seperate_blocks(msg)
		bytetext = b""
		for block in blocks:
			intctext = self.intbullshittery(block)
			byteblock = fn(intctext)
			bytetext += self.unintbullshittery(byteblock)
		return bytetext

	def decrypt(self, msg):
		return self.perform_on_blocks(msg, self.speck.decrypt)

	def encrypt(self, msg):
		return self.perform_on_blocks(msg, self.speck.encrypt)

	def padding_size(self, msg_size):
		return (self.block_size//8) - (msg_size % (self.block_size // 8))

class NoneCipher:
	block_size = 8

	def __init__(self, key):
		pass

	def decrypt(self, msg):
		return msg

	def encrypt(self, msg):
		return msg

	def padding_size(self, msg_size):
		return 0
