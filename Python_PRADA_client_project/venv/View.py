from Observable import Observable
from Observer import Observer

from Model import Model

from typing import List
import curses

from WindowNode import WindowNode

class View(Observable):

    _observers: List[Observer] = []
    userInput = ""

    def __init__(self,model:Model):
        self.model = model

        self.link_clicked = False
        self.link = None
        self.link_dict = {}

        self.inp_field_entered = False
        self.inp_field_tag = None
        self.inp_field = None
        self.inp_field_dict = {}

        self.buttonClicked = False

    def init(self,nlines,ncols):
        self.rootWindowNode = WindowNode(self.model.binWinTree,self.link_dict, self.inp_field_dict)
        self.rootWindow = self.rootWindowNode.getWindow()
        self.cur_window = self.rootWindow
        self.initCurses(nlines,ncols)
        curses.doupdate()
        print("View initialised")

    def initCurses(self,nlines,ncols):
        curses.resize_term(nlines,ncols)
        self.rootWindowNode.maxY, self.rootWindowNode.maxX = self.rootWindow.getmaxyx()

        curses.noecho()
        curses.cbreak()
        curses.mousemask(curses.BUTTON1_CLICKED)
        curses.curs_set(1)  # cursor in this case is the char writing thing not the mouse

        curses.start_color()
        curses.use_default_colors()
        if curses.has_colors():
            curses.init_pair(1, curses.COLOR_WHITE, curses.COLOR_BLACK)
            curses.init_pair(2, curses.COLOR_YELLOW, curses.COLOR_BLACK)

    def checkForClientInput(self):

        input = self.cur_window.getch()
        if(not input == -1):
            #   mouse clicked
            if(input == curses.KEY_MOUSE):
                _, click_column, click_row, _, bstate = curses.getmouse()
                #  self.mouseclick = bstate
                if(bstate == curses.BUTTON1_CLICKED):
                    click_processed = False

                    for key in self.inp_field_dict:
                        value = self.inp_field_dict[key]
                        inp_field_row, inp_field_begin, inp_field_end = value[2],value[3],value[4]
                        if(click_row == inp_field_row and click_column >= inp_field_begin and click_column <= inp_field_end):
                            self.inp_field = value[0]
                            self.inp_field_tag = value[1]
                            self.cur_window = self.inp_field.win
                            click_processed = True
                            break

                    if not click_processed:
                        for key in self.link_dict:
                            value = self.link_dict[key]
                            link_row, link_begin, link_end = value[0],value[1],value[2]
                            if(click_row == link_row and click_column >= link_begin and click_column <= link_end):
                                self.link_clicked = True
                                self.link = key
                                break
                    #If clicked outside of input field loose focus
                    if not click_processed:
                        if not self.inp_field == None:
                            self.inp_field.do_command(curses.ascii.NL)
                            self.cur_window = self.rootWindow
            #  keyboard button pressed
            else:
                if self.inp_field is not None:
                    "Edit in the widget"
                    if(self.inp_field_tag.hide_text and curses.ascii.isprint(input)):
                        input = '*'
                    if not self.inp_field.do_command(input):
                        self.cur_window = self.rootWindow
                        self.inp_field_entered = True

                    self.inp_field.win.refresh()
                else:
                    self.buttonClicked = True
                    self.button = input

            self.notify()

    def attach(self, observer: Observer) -> None:
        print("View: Attached an observer.")
        self._observers.append(observer)

    def detach(self, observer: Observer) -> None:
        print("View: Detached an observer.")
        self._observers.remove(observer)

    def notify(self,) -> None:
        #print("View: Notifying observers!")
        for observer in self._observers:
            observer.update(self)

    def endScreen(self):
        curses.nocbreak()
        self.stdscr.keypad(False)
        curses.echo()
        curses.endwin()
        print("View closed")

