import serial
import time
import sys
import socket
from queue import SimpleQueue
from threading import Thread, Lock

from utils.ByteUtils import *

from messages.SRVMessage import SRVMessage
from messages.ClientMessage import ClientMessage
from gucci import GucciHeader, NoneHeader
from cipher import Cipher, NoneCipher
import otway_rees

class Connection:
    portOpened = False
    recInProgress = False

    def __init__(self, serialPort, port):
        self.or_key = 0x000000000000000000000000000000000000000000000000000000000000000b
        self.cipher = NoneCipher(0)
        self.header = NoneHeader

        self.switch_socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        self.switch_socket.connect(("localhost", 1234))
        self.switch_socket_file = self.switch_socket.makefile("rwb")

        self.gateway_socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        self.gateway_socket.connect(("localhost", 1236))
        self.gateway_socket_file = self.gateway_socket.makefile("rwb")


        print("Try to open port")
        try:
            print("No serial port required")
        except serial.SerialException:
            print("Trouble opening port. Maybe device could not be found")
        else:
            self.portOpened = True
            print("Port opened successfully")
            self.serLock = Lock()

    def enable_cipher(self, key):
        print("Enabling Cipher")
        self.cipher = Cipher(key)
        self.header = GucciHeader
        print("New Key: \n\t", int.to_bytes(key,32, byteorder="big").hex())

    def receive_or_session_key(self, ra):
        print("Receiving Session Key")
        msg_id_byte = self.gateway_socket_file.read(1)
        msg_id = int.from_bytes(msg_id_byte, byteorder="big")
        if not msg_id:
            print("No Connection to Gateway")

        if msg_id != otway_rees.or_session_key:
            print("Unexpected Message, not Session Key")
            print("Got: ", msg_id)
            quit(0)

        data = self.gateway_socket_file.read(48)
        if not data:
            print("No Connection to Gateway")
            quit(0)

        ks_bytes = otway_rees.read_session_key(self.or_key, ra, data)
        print("Received Session Key from Gateway")
        return int.from_bytes(ks_bytes, byteorder="big")

    def outputLoop(self, message : ClientMessage):
        if not message == None:
            #build msg

            msg_size = len(message.to_bytes())
            # determine padding size
            padding_size = self.cipher.padding_size(self.header.header_size + msg_size)
            full_msg = self.header(msg_size, padding_size).to_bytes() + message.to_bytes() + (b"\00" * padding_size)
            full_msg = self.cipher.encrypt(full_msg)

            noBytesSend = self.switch_socket_file.write(full_msg)
            self.switch_socket_file.flush()

            print("Bytes to SRV!    #: " + str(noBytesSend) + ", Hex.: " + getHexString(
                message.to_bytes()) + ", Bin.: " + getBinString(message.to_bytes()))
            sys.exit()

    def inputLoop(self,inputqueue: SimpleQueue):
        while True:
            prada_msg = self.receive_gucci_msg()
            # print("received message:")
            # print(prada_msg.hex())
            srv_message = self.handle_prada_message(prada_msg)
            inputqueue.put(srv_message)

    def receive_gucci_msg(self):
        gucci_header_size = GucciHeader.header_size
        gucci_block_size = Cipher.block_size // 8
        #get first block
        firstblock = self.switch_socket_file.read(gucci_block_size)
        firstblockplain = self.cipher.decrypt(firstblock)

        print("block size:\n\t" + str(gucci_block_size))
        print("first block cipher:\n\t" + firstblock.hex())
        print("cipher bytes:\n\t" + str(len(firstblock)))
        print("first block plain:\n\t" + firstblockplain.hex())
        print("plaintext bytes:\n\t" + str(len(firstblockplain)))

        header = self.header.from_bytes(firstblockplain)
        print("received ", header.size, " bytes")

        #read remaining msg
        remaining = header.size + header.padding - (gucci_block_size - gucci_header_size)
        remainingblocks = self.switch_socket_file.read(remaining)

        remainingblocksplain = self.cipher.decrypt(remainingblocks)

        fullmsg = firstblockplain + remainingblocksplain

        return fullmsg[gucci_header_size:len(fullmsg)-header.padding]

    def handle_prada_message(self, msg):
        protocol_fields = bytearray()
        varInt_payload_size = bytearray()
        payload = bytearray()
        rec_prot_fields = True
        rec_payload_size = True
        rec_payload = True

        msgType = 0
        leftToRead = 0
        self.recInProgress = False

        for oneByte in msg:
            # print(getHexString([oneByte]) + "" + getBinString([oneByte]))

            #  If this is the first byte of a new message:
            if (not self.recInProgress):
                self.recInProgress = True
                rec_prot_fields = False

                msgType = oneByte >> 4
                leftToRead = self.getDefaultSize(msgType, oneByte)
                print("Reveiving msg of type: " + str(msgType))

                if (msgType == 0 or msgType == 3 or msgType == 5):
                    print("Message can have payload! ")
                    rec_payload_size = False  # always given as VarInt

            #  First receive protocol fields including payload size
            if (not rec_prot_fields):
                # print(getBinString([oneByte]))
                protocol_fields.append(oneByte)
                leftToRead -= 1
                # print(str(leftToRead) + " " + getHexString(protocol_fields))

                #  All protocol fields are read, except maybe for last Varint payload size
                if (leftToRead == 0):

                    #  If rec_payload_size is set, the last byte always belongs to payload size, but there may be more (Varint)
                    if (not rec_payload_size):
                        # print("Reading payload size!")
                        varInt_payload_size.append(oneByte)
                        if (oneByte >> 7) == 1:
                            leftToRead = 1
                        else:
                            rec_prot_fields = True
                            rec_payload_size = True
                            leftToRead = int_from_varint(varInt_payload_size)
                            print("Payloadsize: " + str(leftToRead))

                        #  leftToRead is the payload size now
                        if (leftToRead > 0):
                            rec_payload = False
                        else:
                            self.recInProgress = False
                    # no payload to read, last byte is not part of payload size, we done here
                    else:
                        print("Only read protocol fields, we done")
                        rec_prot_fields = True
                        self.recInProgress = False

            #  still need to read payload? Do it here
            elif (not rec_payload):
                payload.append(oneByte)
                leftToRead -= 1
                if (leftToRead == 0):
                    rec_payload = True
                    self.recInProgress = False

            if (not self.recInProgress):
                print("Bytes to Client! #: " + str(len(protocol_fields) + len(payload)) +
                      ", Hex.: " + getHexString(protocol_fields) + getHexString(payload) +
                      ", Bin.: " + getBinString(protocol_fields) + " " + getBinString(payload))

                return SRVMessage(msgType, protocol_fields, payload)


    # Number of protocol fields excluding payload, including first byte of payload size
    def getDefaultSize(self, msgType, firstByte):
        size = 0
        if msgType == 0:
            #print("First byte: " + getBinString([firstByte]))
            if((firstByte & 4) >> 2 == 1):
                size = 7
            else:
                size = 9
        elif msgType == 3: #  UI Update
            if (firstByte & 8) >> 3 == 1:
                size = 6
            else:
                size = 4
        elif msgType == 10:  # Noop_ACK
            size = 3
        elif msgType == 11:  #  Heartbeat
            size = 3
        elif msgType == 12:  #  Con Term.
            if (firstByte & 8) >> 3 == 1:
                size = 5
            else:
                size = 3
        elif msgType == 13:  # ERROR
            if (firstByte & 8) >> 3 == 1:
                size = 5
            else:
                size = 3
        #print("Protocol fields: " + str(size) + " bytes")
        return size

