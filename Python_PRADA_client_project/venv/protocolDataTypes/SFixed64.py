from protocolDataTypes.PDT import DataType


class SFixed64(DataType):

    def __init__(self, byte_representation: bytearray):
        self.data_type_id = DataType.SFIXED64
        self.byte_representation = byte_representation

    def decode(self):
        return int.from_bytes(self.byte_representation, byteorder='little', signed = True)

    def to_string(self):
        return str(self.decode())