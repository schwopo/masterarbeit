from protocolDataTypes.PDT import DataType


class Boolean(DataType):

    def __init__(self, byte_representation: bytearray):
        self.data_type_id = DataType.BOOLEAN
        self.byte_representation = byte_representation

    def decode(self):
        return self.byte_representation[0] == 1

    def to_string(self):
        return str(self.decode())
