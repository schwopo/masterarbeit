from protocolDataTypes.PDT import DataType
from utils.ByteUtils import int_from_varint

class Uint64(DataType):

    def __init__(self, byte_representation: bytearray):
        self.data_type_id = DataType.UINT64
        self.byte_representation = byte_representation

    def decode(self):
        return int_from_varint(self.byte_representation)

    def to_string(self):
        return str(self.decode())

