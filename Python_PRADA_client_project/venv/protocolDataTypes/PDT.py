from __future__ import annotations
from abc import *
import struct


class DataType( ABC ):
    UFIXED32    = 0
    SFIXED32    = 1
    FLOAT       = 2
    UFIXED64    = 1000
    SFIXED64    = 1001
    DOUBLE      = 1002 # not used
    BOOLEAN     = 2000
    UINT32      = 2001
    UINT64      = 2002
    SINT32      = 2003
    SINT64      = 2004
    STRING      = 3000
    BYTES       = 3001

    data_type_id = None
    byte_representation = bytearray()

    def to_bytes( self ) -> bytearray:
        return self.byte_representation

    @abstractmethod
    def decode( self ):
        pass

    @abstractmethod
    def to_string( self ):
        pass

def byte_rep_from_string( pdt_id, string ):
    byte_rep = bytearray()
    if( pdt_id == DataType.UFIXED32):
        number = int(float(string))
        for byte in number.to_bytes(4, byteorder = 'little', signed = False):
            byte_rep.append(byte)
    elif( pdt_id == DataType.SFIXED32 ):
        number = int(float(string))
        for byte in number.to_bytes(4, byteorder = 'little', signed = True):
            byte_rep.append(byte)
    elif( pdt_id == DataType.FLOAT ):
        number = float(string)
        byte_rep = bytearray(struct.pack("f", number))
    elif( pdt_id == DataType.UFIXED64 ):
        number = int(string)
        for byte in number.to_bytes(8, byteorder = 'little', signed = False):
            byte_rep.append(byte)
    elif( pdt_id == DataType.SFIXED64 ):
        number = int(string)
        for byte in number.to_bytes(8, byteorder = 'little', signed = True):
            byte_rep.append(byte)
    elif( pdt_id == DataType.DOUBLE ):
        number = float(string)
        byte_rep = bytearray(struct.pack("d", number))
    return byte_rep