from protocolDataTypes.PDT import DataType


class Bytes(DataType):

    def __init__(self, byte_representation: bytearray):
        self.data_type_id = DataType.BYTES
        self.byte_representation = byte_representation

    def decode(self):
        return self.byte_representation

    def to_string(self):
        return str(self.byte_representation)
