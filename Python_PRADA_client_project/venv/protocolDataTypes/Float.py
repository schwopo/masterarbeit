from protocolDataTypes.PDT import DataType
import numpy as np

class Float(DataType):

    def __init__(self, byte_representation: bytearray):
        self.data_type_id = DataType.FLOAT
        self.byte_representation = byte_representation

    def decode(self):
        data_bytes = np.array(self.byte_representation, dtype = np.uint8)
        data_as_float = data_bytes.view(dtype = np.float32)
        return data_as_float

    def to_string(self):
        return str(self.decode()[0])
