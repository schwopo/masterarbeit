from protocolDataTypes.PDT import DataType
from utils.ByteUtils import int_from_varint

class UFixed32(DataType):

    def __init__(self, byte_representation: bytearray):
        self.data_type_id = DataType.UFIXED32
        self.byte_representation = byte_representation

    def decode(self):
        return int.from_bytes(self.byte_representation, byteorder='little', signed = False)

    def to_string(self):
        return str(self.decode())
