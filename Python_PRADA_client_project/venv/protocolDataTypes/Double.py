from protocolDataTypes.PDT import DataType
import numpy as np

#  Arduino is not capable of 64 bit floating point numbers. We use only the Float class for the time being.
class Double(DataType):

    def __init__(self, byte_representation: bytearray):
        self.data_type_id = DataType.DOUBLE
        self.byte_representation = byte_representation

    def decode(self):
        data_bytes = np.array(self.byte_representation, dtype = np.uint8)
        data_as_float = data_bytes.view(dtype = np.float64 )
        return data_as_float

    def to_string(self):
        return str(self.decode()[0])
