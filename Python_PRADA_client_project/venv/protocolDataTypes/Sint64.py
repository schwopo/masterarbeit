from protocolDataTypes.PDT import DataType
from utils.ByteUtils import int_from_varint, decode_zigzag_64bit

class Sint64(DataType):

    def __init__(self, byte_representation: bytearray):
        self.data_type_id = DataType.SINT64
        self.byte_representation = byte_representation

    def decode(self):
        return decode_zigzag_64bit(int_from_varint(self.byte_representation))

    def to_string(self):
        return str(self.decode())
