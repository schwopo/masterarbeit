from protocolDataTypes.PDT import DataType


class String(DataType):

    def __init__(self, byte_representation: bytearray):
        self.data_type_id = DataType.STRING
        self.byte_representation = byte_representation

    def decode(self):
        return self.byte_representation.decode('utf-8')

    def to_string(self):
        return self.decode()
