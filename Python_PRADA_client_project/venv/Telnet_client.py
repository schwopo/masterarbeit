from telnetlib import Telnet
from queue import SimpleQueue
from time import sleep

class Telnet_client:

    #def __init__(self,):



    def update_telnet_server(self, q:SimpleQueue):
        tn = Telnet("localhost", 23)
        sleep(1)
        tn.read_until(b"Username: ")
        tn.write(b"Admin" + b"\r\n")
        tn.read_until(b"Password: ")
        tn.write(b"123" + b"\r\n")
        tn.read_until(b'>')
        # Ab hier sind wir am command prompt

        while True:
            if q.qsize() > 0:
                content_lines = q.get()
                for line in content_lines:
                    tn.write(line.encode('ascii'))
                    tn.read_until(b'>')


'''import asyncio, telnetlib
from View import View

from queue import SimpleQueue
from threading import Thread

class TELNET:

    def __init__(self, telnet_queue:SimpleQueue):
        self.inputThread = Thread(target=self.telnet_client_thread, args=(telnet_queue,))
        self.inputThread.setDaemon(True)
        self.inputThread.start()

    def telnet_client_thread(self, telnet_queue:SimpleQueue):
        #loop = asyncio.get_event_loop()

        loop = asyncio.new_event_loop()
        asyncio.set_event_loop(loop)
        coro = telnetlib3.open_connection('localhost', 6023, shell=self.shell)
        reader, writer = loop.run_until_complete(coro)
        loop.run_until_complete(writer.protocol.waiter_closed)

    @asyncio.coroutine
    def shell(self, reader, writer):
        while True:
                writer.write("Hallo" + '\n')
                yield from writer.drain()
                writer.write("Hallo" + '\n')
                yield from writer.drain()
                writer.write("Hallo" + '\n')
                yield from writer.drain()
'''