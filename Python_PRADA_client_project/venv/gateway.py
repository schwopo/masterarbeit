import time
import tkinter

import serial
import sys
import socket
import random
import otway_rees
from cipher import Cipher
from ecdsa import ECDH, SECP256k1
import hashlib

coupling_static_pubk_request = 1
coupling_static_pubk = 2
coupling_ephemeral_pubk_request = 3
coupling_ephemeral_pubk = 4
coupling_shared_secret = 5

ec_pubkey_size = 64
session_key = b""

received_pubk = b""

def perform_dh(msg_code, encrypt=False, decrypt=False, id=0):
    global received_pubk
    ecdh = ECDH(curve=SECP256k1)
    ecdh.generate_private_key()
    local_public_key = ecdh.get_public_key()
    public_key_bytes = local_public_key.to_string()
    if encrypt: #we also include id for server here, also padding
        cipher = Cipher(int.from_bytes(session_key, byteorder="big"))
        public_key_bytes += id.to_bytes(4, byteorder="big")
        public_key_bytes += b"\0" * cipher.padding_size(len(public_key_bytes))
        public_key_bytes = cipher.encrypt(public_key_bytes)

    #send key
    msg_code_byte = int.to_bytes(msg_code, 1, byteorder="big")
    # switch_socket.send(msg_code_byte + public_key_bytes)

    switch_socket.send(msg_code_byte)
    time.sleep(0.1)
    switch_socket.send(public_key_bytes)

    # receive key
    received_msg_code_byte = switch_socket.recv(1)
    received_msg_code = int.from_bytes(received_msg_code_byte, byteorder="big")
    remote_public_key = switch_socket.recv(ec_pubkey_size)

    if decrypt:
        cipher = Cipher(int.from_bytes(session_key, byteorder="big"))
        remote_public_key = cipher.decrypt(remote_public_key)

    received_pubk = remote_public_key
    ecdh.load_received_public_key_bytes(remote_public_key)

    secret = ecdh.generate_sharedsecret_bytes()
    hashbytes = hashlib.sha256(secret)
    return hashbytes.digest()

def verify_key(key):
    print("Is this the key that was delivered with your device?")
    print(key.hex())
    choice = input("(y/n)")
    if choice.lower() in ["y", "yes"]:
        return True
    else:
        return False

def upload_shared_secret():
    global session_key
    global received_pubk
    session_key = perform_dh(coupling_static_pubk_request)
    choice = verify_key(received_pubk)
    if choice == False:
        exit()
    id = 2
    shared_key_bytes = perform_dh(coupling_ephemeral_pubk_request, encrypt=True, decrypt=True, id=id)
    print("Uploaded secret")
    shared_key = int.from_bytes(shared_key_bytes, byteorder="big")
    otway_rees.keys[id] = shared_key
    write_key_db("keydb.txt")
    print("Saved secret")




def load_key_db(filename):
    with open(filename, "r") as keydb:
        lines = keydb.readlines()
        for line in lines:
            if line == "":
                continue
            words = line.split(" ")
            id = int(words[0])
            key = words[1]
            keybytes = bytes.fromhex(key)
            otway_rees.keys[id] = int.from_bytes(keybytes, byteorder="big")

def write_key_db(filename):
    with open(filename, "w") as keydb:
        for id in otway_rees.keys:
            key = otway_rees.keys[id]
            keystring = key.to_bytes(32, byteorder="big").hex()
            line = "{id} {key}\n".format(id = id, key = keystring)
            keydb.write(line)

key_size_bytes = 32

load_key_db("keydb.txt")
print(otway_rees.keys)

client_id = 1

server_id = 2

switch_port = 1235
gateway_port = 1236

upload_mode = False

if (len(sys.argv) > 1 and sys.argv[1] == "upload"):
    upload_mode = True

switch_socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
gateway_server_socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)

print("Started Gateway connecting to switch port " + str(switch_port))
switch_socket.connect(("localhost", switch_port))

if not upload_mode:
    print("Awaiting Connection from client on port " + str(gateway_port))
    gateway_server_socket.bind(("localhost", gateway_port))
    gateway_server_socket.listen(5)
    (client_socket, address) = gateway_server_socket.accept()
    print("Client connected")
    while True:
        print("Awaiting Session Key Request...")
        msg_id_byte = switch_socket.recv(1)
        msg_id = int.from_bytes(msg_id_byte, byteorder="big")
        if not msg_id:
            print("No Connection to Switch")

        if msg_id != otway_rees.or_session_key_request:
            print("Received Message of Type", msg_id)
            print("Unexpected Message, not Session Key Request")
            quit(0)
        data = switch_socket.recv(40)
        if not data:
            quit(0)

        print("Received data")
        a, b, ra, rb = otway_rees.read_session_key_request(data)
        received_client_id = a
        received_server_id = b
        client_nonce = ra
        server_nonce = rb
        print("Got Session Key Request")

        print("Generating Session Key")
        session_key = random.randbytes(key_size_bytes)

        print("Sending Session Key to Server")
        server_key = otway_rees.keys[server_id]
        sk_msg_server = otway_rees.generate_session_key_msg(server_key, server_nonce, session_key)
        switch_socket.send(sk_msg_server)
        print("Sent Session Key to Server")

        print("Sending Session Key to Client")
        client_key = otway_rees.keys[client_id]
        sk_msg_client = otway_rees.generate_session_key_msg(client_key, client_nonce, session_key)
        client_socket.send(sk_msg_client)
        print("Sent Session Key to Client")

if upload_mode:
    upload_shared_secret()
