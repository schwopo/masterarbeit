import curses
import curses.textpad

from BinTree import TreeNode
from Observable import Observable
from Observer import Observer

from windowcontents.Content import Content
from windowcontents.TextContent import TextContent
from windowcontents.JumpToTag import JumpToTag

class WindowNode(Observer):

    def __init__(self, treeNode, link_dic, inp_field_dic,  parentNode = None, splitDir = None, reqSize = 0 , relPos = 0):
        #attach to corresponding data in model
        self.treeNode = treeNode
        self.treeNode.attach(self)

        #set values of this window
        self.isLeaf = True
        self.splitDir = None
        self.id = treeNode.id
        self.childNode1 = None #left node
        self.childNode2 = None #right node
        self.content: Content = treeNode.content

        self.link_dic = link_dic
        self.inp_field_dic = inp_field_dic

       #Init curses window wrapped with this class
        if parentNode == None: #not a child so i must be the rootWindow -> Init curse screen
            self.isRoot = True
            self.window = curses.initscr()
            self.window.nodelay(True)
            self.window.keypad(True)
            self.global_y, self.global_x = 0, 0
        else:
            if (splitDir == TreeNode.SPLIT_VERTICAL):
                    self.window = parentNode.getWindow().derwin(parentNode.maxY, reqSize, 0, relPos)
                    self.global_y, self.global_x = parentNode.global_y, parentNode.global_x + relPos
            else:
                    self.window = parentNode.getWindow().derwin(reqSize, parentNode.maxX, relPos, 0)
                    self.global_y, self.global_x = parentNode.global_y + relPos, parentNode.global_x

        self.maxY, self.maxX = self.window.getmaxyx()
        self.displayContent()

    #if id not in the tree nothing happens as of yet
    def splitWindow(self, splitDir, splitMargin, firstNode, sndNode):
        self.isLeaf = False
        self.splitDir = splitDir
        self.window.clear()
        self.content = None
        if (splitDir == TreeNode.SPLIT_VERTICAL):
            self.childNode1 = WindowNode(firstNode, self.link_dic, self.inp_field_dic, self, splitDir, splitMargin)
            self.childNode2 = WindowNode(sndNode,   self.link_dic, self.inp_field_dic, self, splitDir, self.maxX - splitMargin, splitMargin)
        else:
            self.childNode1 = WindowNode(firstNode, self.link_dic, self.inp_field_dic, self, splitDir, splitMargin)

            self.childNode2 = WindowNode(sndNode,   self.link_dic, self.inp_field_dic, self, splitDir, self.maxY - splitMargin, splitMargin)

    def getWindow(self):
        return self.window

    #curses.doupdate has to be called after this.
    def displayContent(self):
        if self.isLeaf:
            if not self.treeNode.keep_content:
                self.window.clear() #später mit flag
            self.window.box()  #überschreibt jedes mal rand auch bei update
            x_pos = y_pos = 0

            if(not self.content == None):
                for item in self.content:
                    if (item.content_id == Content.JUMP_FOR):
                        if(item.jumpRows and item.jumpColumns):
                            self.window.move(y_pos + item.rows, item.columns)
                        elif(item.jumpRows):
                            self.window.move(y_pos + item.rows, 0)
                        elif(item.jumpColumns):
                            self.window.move(y_pos, x_pos + item.columns)

                    elif(item.content_id == Content.JUMP_TO):
                        self.window.move(item.row,item.column)

                    elif(item.content_id == Content.LINK):
                        row, x_begin = self.global_y + y_pos, self.global_x + x_pos
                        self.window.addstr(y_pos, x_pos, item.display_value, curses.A_STANDOUT)
                        _, new_x = self.window.getyx()
                        x_end = self.global_x + new_x - 1
                        self.link_dic[item.link_id] = (row,x_begin,x_end)

                    elif(item.content_id == Content.CONSTANT_DF):
                        self.window.addstr(item.display_value)

                    elif (item.content_id == Content.INPUT_FIELD):
                        row, x_begin = self.global_y + y_pos, self.global_x + x_pos
                        self.window.addstr(y_pos, x_pos, "[")
                        if(item.end_of_line):
                            input_field_win = self.window.derwin(1, self.maxX - x_pos - 2, y_pos, x_pos + 1)
                            x_pos = self.maxX - 1
                        else:
                            input_field_win = self.window.derwin(1, item.field_width, y_pos , x_pos + 1)
                            x_pos += item.field_width + 1

                        if (item.dfault_text):
                            input_field_win.addstr(item.pdt_string.to_string())
                        input_field = curses.textpad.Textbox(input_field_win)

                        self.window.addstr(y_pos, x_pos, "]")
                        if(item.end_of_line):
                            self.window.move(row, self.maxX - 1)

                        y, new_x = self.window.getyx()
                        if ( item.end_of_line ):
                            x_end = self.global_x + new_x
                        else:
                            x_end = self.global_x + new_x - 1

                        self.inp_field_dic[ item.input_field_id ] = ( input_field, item, row, x_begin, x_end )

                    elif item.content_id == Content.TEXT:
                        self.window.addstr( y_pos, x_pos, item.textData.decode( 'utf-8' ) )

                    y_pos, x_pos = self.window.getyx()

            self.window.noutrefresh()


    def changeContent( self, content : [ Content ] ):
            if not self.isLeaf: #I have children that need to be erased.
                self.unsplit()
            self.content = content
            #print(content)
            self.displayContent() #clears the space of this window completly including children

    def unsplit(self):
        self.isLeaf = True
        self.splitDir = None
        self.childNode1 = None
        self.childNode2 = None
        self.window.clear()

    def printTree(self):
        print(self.getTreeString())

    def getTreeString(self):
        treeString = ""
        if self.isLeaf:
            treeString = str(self.id)
        else:
            if self.splitDir == WindowNode.SPLIT_VERTICAL:
                splitStr = "V"
            else:
                splitStr = "H"
            treeString = "[ " + self.childNode1.getTreeString() + " <-- (" + str(self.id) + ":" + splitStr + ") --> " +self.childNode2.getTreeString() + " ]"
        return treeString

    def update(self, observable: Observable) -> None:
        if(self.treeNode.contentChanged):
            self.changeContent(self.treeNode.content)
            self.treeNode.contentChanged = False
        if(self.treeNode.wasSplit):
            tn = self.treeNode
            self.splitWindow(tn.splitDir, tn.splitMargin, tn.childNode1, tn.childNode2)
            tn.wasSplit = False
        #print("View: Reacted to the event")

