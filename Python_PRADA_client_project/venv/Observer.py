from __future__ import annotations
from abc import ABC, abstractmethod

class Observer(ABC):
    """
    The Observer interface declares the update method, used by observables.
    """

    @abstractmethod
    def update(self, observable: Observable) -> None:
        """
        Receive update from subject.
        """
        pass