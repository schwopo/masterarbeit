from Observable import Observable
from Observer import Observer

from Model import Model
from View import View

import sys
import time
from queue import SimpleQueue
from threading import Thread
import curses

from protocolDataTypes.PDT import byte_rep_from_string
from utils.ByteUtils import getBinString

from ConnectionManager import ConnectionManager
from messages import *

from Telnet_client import Telnet_client

class Controller(Observer):

    def __init__(self, model, view):
        print("Controller initialised")
        self.model = model
        self.view = view
        self.view.attach(self)

        self.cm = ConnectionManager()

        self.want_ui = True
        self.protocol_version = 1

        #Optional UI params:
        self.req_win_size   = True
        self.req_rows       = 64
        self.req_columns    = 64

        # '' Enable for Telnet testing
        #self.tn = Telnet_client()

        while 1:
            self.mainloop()

    def mainloop(self):
        if not self.cm.connection_closed:
            self.cm.checkForSRVInput()
            if(not self.cm.connected and not self.cm.need_ACK):
                self.cm.connect(self.protocol_version, self.want_ui, self.req_win_size, self.req_rows, self.req_columns)

            if(self.cm.gotInput):
                input = self.cm.lastInput
                self.handleInput(input)
                self.cm.gotInput = False

            if(self.cm.connected and self.want_ui):
                self.view.checkForClientInput()

        else:
            if hasattr(self.view, 'rootWindow'):
                self.deinitcurses()
            self.cm.to_default()
            # sys.exit(0)

    def handleInput(self,input:SRVMessage):
        if input.message_id == 0: #  Con. init.
            self.cm.send_noop_ack()
            self.cm.srv_timeout = input.protocol_fields[5]
            self.cm.msg_timeout = self.cm.srv_timeout  / self.cm.max_sending_tries  #  Has to be decided by the client.
            print(str( self.cm.srv_timeout ) + " " + str( self.cm.msg_timeout ) )

            if(self.want_ui):
                if((input.protocol_fields[0] & 4) >> 2 == 0): # SRV has own size in mind:
                    self.view.init(input.protocol_fields[6],input.protocol_fields[7])
                    ''' Enable for Telnet testing
                    self.telnet_queue = SimpleQueue()
                    self.inputThread = Thread(target=self.tn.update_telnet_server, args=(self.telnet_queue,))
                    self.inputThread.setDaemon(True)
                    self.inputThread.start()
                    '''
                else:
                    pass
                    # TODO Init mit standardsize
                
                if not len(input.payload) == 0: # No UI given
                    self.model.updateUIStructure(input.payload)
                    curses.doupdate()
                else: # We wanted a UI but did not get one but are connected. So no UI it is^^
                    self.want_ui = False

                # '' Enable for Telnet testing
                #self.telnet_queue.put( self.get_window_content())

        elif input.message_id == 3: #  UI Update
            self.cm.send_noop_ack()
            self.model.updateUIStructure(input.payload)
            curses.doupdate()

            # '' Enable for Telnet testing
            #self.telnet_queue.put( self.get_window_content())

        elif input.message_id == 10:  # Noop_ACK. Only on con_term atm
            pass
        elif input.message_id == 11: #  Heartbeat
            print("Heartbeat")
            self.cm.send_noop_ack()
        elif input.message_id == 12:  # Con Term.
            self.cm.send_noop_ack()
            print("SRV terminated con!")
            time.sleep(2)
            self.cm.to_default()
            if hasattr(self.view, 'rootWindow'):
                self.deinitcurses()
        elif input.message_id == 13: # Error
            if hasattr(self.view, 'rootWindow'):
                self.deinitcurses()
            sys.exit(0)

    def update(self, observable: Observable) -> None:
        if(self.view.link_clicked):
            self.cm.send_link_clicked(self.view.link)
            self.view.link_clicked = False
        elif(self.view.inp_field_entered):
            byte_rep = byte_rep_from_string( self.view.inp_field_tag.pdt_id, self.view.inp_field.gather() )
            self.cm.send_user_input( self.view.inp_field_tag.input_field_id, byte_rep )
            self.view.inp_field_entered = False
            self.view.inp_field = None
            self.view.inp_field_tag = None
        if(self.view.buttonClicked):
            if self.view.button == ord('q'):
                self.deinitcurses()
            self.view.buttonClicked = False

    def deinitcurses(self):
        curses.nocbreak()
        self.view.rootWindow.keypad(False)
        curses.echo()
        curses.endwin()

    #Used for the TElnet test but may be useful for saving whole window content for resizing
    def get_window_content(self):
        maxY, maxX = self.view.rootWindow.getmaxyx()
        content_lines = []
        lines = ""
        for y in range(0, maxY):
            for x in range(0, maxX):
                ch = self.view.rootWindow.inch(y, x)

                #if (ch & 65536)  == curses.A_ALTCHARSET:
                #    lines += chr((ch & 255) + 83)
                #else:
                lines += chr( ch & 255)

            lines = lines.rstrip()
            lines += "\r\n"

            content_lines.append(lines)
            lines = ""

        return content_lines