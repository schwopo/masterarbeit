from Observable import Observable
from Observer import Observer
from typing import List

class TreeNode(Observable):

    _observers: List[Observer] = []
    SPLIT_HORIZONTAL = 0
    SPLIT_VERTICAL = 1

    def __init__(self, id, content = None):
        #print("Node created, id: " + str(id))
        self.id = id
        self.isLeaf = True
        self.splitDir = None
        self.content = content  # should be bytearray
        self.childNode1 = None #left node
        self.childNode2 = None #right node

        self.wasSplit = False
        self.keep_content = False
        self.contentChanged = False

    #if id not in the tree nothing happens as of yet
    def split(self, id, splitDir, splitMargin, firstNode, sndNode):
        if (not self.id == id):
            if not self.isLeaf:
                self.childNode1.split(id, splitDir, splitMargin, firstNode, sndNode)
                self.childNode2.split(id, splitDir, splitMargin, firstNode, sndNode)
        else:
            self.isLeaf = False
            self.splitDir = splitDir
            self.splitMargin = splitMargin
            self.content = None
            self.keep_content = False
            self.childNode1 = firstNode
            self.childNode2 = sndNode
            self.wasSplit = True
            self.notify()

    def changeContent(self, id, content, keep_content):
        if (not self.id == id):  #pass on the content if the nodes has children, does not prevent multiple windows with same id.
            if(not self.isLeaf):
                self.childNode1.changeContent(id, content, keep_content)
                self.childNode2.changeContent(id, content, keep_content)
        else: #This is my id. change my content and display it
            if not self.isLeaf: #I have children that need to be erased.
                self.unsplit()
            self.keep_content = keep_content
            self.content = content
            self.contentChanged = True
            self.notify()

    def unsplit(self):
        self.isLeaf = True
        self.splitDir = None
        self.childNode1 = None
        self.childNode2 = None

    def hasChild(self,id):
        if(self.isLeaf):
            return False
        else:
            if(self.childNode1.id == id or self.childNode2.id == id):
                #print("node " + str(self.id) +  " has child " + str(id))
                return True
            else:
                return self.childNode1.hasChild(id) or self.childNode2.hasChild(id)

    #  Return ID of the next node in the binary tree from the node with id, not couting parent nodes of the node asked
    def nextNodeLeftBound(self, id):
        if(not self.isLeaf):
            if(self.id == id):
                return self.childNode1.id
            elif(self.childNode1.id == id):
                if self.childNode1.isLeaf:
                    return self.childNode2.id
                else:
                    return  self.childNode1.nextNodeLeftBound(id)
            elif(self.childNode2.id == id and self.childNode2.isLeaf):
                return -1
            else: #  None of my children has the id and is a leaf
                if self.childNode1.hasChild(id):
                    nextNode = self.childNode1.nextNodeLeftBound(id)
                    if nextNode == -1:
                        return self.childNode2.id
                    else:
                        return nextNode
                else:
                    return self.childNode2.nextNodeLeftBound(id)
        else:
            print(str(self.id) + " I am a Leaf, you cant call this on me! " + str(id))

    def printTree(self):
        print(self.getTreeString())

    def getTreeString(self):
        treeString = ""
        if self.isLeaf:
            treeString = str(self.id) +": " + self.content
        else:
            split = ""
            if self.splitDir == TreeNode.SPLIT_VERTICAL:
                splitStr = "V"
            else:
                splitStr = "H"
            treeString = "[ " + self.childNode1.getTreeString() + " <-- (" + str(self.id) + ":" + splitStr + ") --> " +self.childNode2.getTreeString() + " ]"
        return treeString

    def attach(self, observer: Observer) -> None:
        #print("Node: " + str(self.id) +"Attached an observer.")
        self._observers.append(observer)

    def detach(self, observer: Observer) -> None:
        print("Model: Detached an observer.")
        self._observers.remove(observer)

    def notify(self) -> None:
        #print("Model: Notifying observers!")
        for observer in self._observers:
            observer.update(self)
