import serial
import threading
import socket
import time

client_port = 1234
gateway_port = 1235

client_id = 0
gateway_id = 1
debug_id = 2
debug_ascii_id = 3
noclient = False

server = serial.Serial("COM4")
logfile = open("switchlog.txt", "w")

def log(sender, recipient, size):
    logfile.write(sender + "\t" + recipient + "\t" + str(size) + "\n")
    logfile.flush()

def bridge_to_server(sock, name):
    if not sock:
        print(name, "not connected")
        return
    connection_closed = False
    while not connection_closed:
        data = sock.recv(1024)
        if data:
            print(name + ": Received data:")
            print(data.hex())
            server.write(data)
            print("Wrote to server")
            log(name, "Server", len(data))
        else:
            print(name + ": Connection closed")
            connection_closed = True

def bridge_from_server():
    while True:
        header = server.read(5)
        recipient = int.from_bytes(header[0:1], byteorder="big")
        size = int.from_bytes(header[1:5], byteorder="big")
        print("Message for", recipient, "with", size, "bytes")
        data = server.read(size)
        if recipient == client_id:
            cs.send(data)
            print("\t",data.hex())
            log("Server", "Client", len(data))

        if recipient == gateway_id:
            gw.send(data)
            print("\t",data.hex())
            log("Server", "Gateway", len(data))

        if recipient == debug_id:
            print("Debug info:")
            print("\t",data.hex())

        if recipient == debug_ascii_id:
            print("Debug Message:")
            print("\t",data.decode("ascii"))

def connect(port):
    s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    s.bind(("localhost", port))
    print("Started Switch on port " + str(port))
    s.settimeout(3)
    s.listen(5)
    try:
        (cs, cs_address) = s.accept()
    except socket.timeout:
        print("Connection timed out")
        cs = None
    return cs

while True:
    gw = connect(gateway_port)
    cs = connect(client_port)

    stc = threading.Thread(target=bridge_from_server, args=())
    cts = threading.Thread(target=bridge_to_server, args=(cs,"Client"))
    gts = threading.Thread(target=bridge_to_server, args=(gw,"Gateway"))

    stc.start()
    cts.start()
    gts.start()
    print("Threads Started")

    stc.join()
    cts.join()
    gts.join()

