import random
from sys import byteorder
from cipher import Cipher
import time
from messages.RawMessage import RawMessage

nonce_size = 4

keys = {}

msg_codes = {}

or_connection_request = 10
or_session_key_request = 20
or_session_key = 30



def generate_connection_request(a, b, ra, k):
    # msg_id, a,b,r,ka(a,b,r,ra)
    r = random.randbytes(nonce_size)

    msg_id = int.to_bytes(or_connection_request, length=1, byteorder="big")
    ba = int.to_bytes(a, length=4, byteorder="big")
    bb = int.to_bytes(b, length=4, byteorder="big")

    plainpart = msg_id + ba + bb + r
    ciphpart = ba + bb + r + ra

    ciph = Cipher(k)
    ciphpart = ciph.encrypt(ciphpart)

    return RawMessage(plainpart + ciphpart)


def generate_session_key_request(own_id, a, b, r, ra):
    # msg_id, a,ka(a,b,r,ra)

    msg_id = int.to_bytes(or_session_key_request, length=1, byteorder="big")
    ba = int.to_bytes(a, length=4, byteorder="big")
    bb = int.to_bytes(b, length=4, byteorder="big")
    bown_id = int.to_bytes(own_id, length=4, byteorder="big")

    plainpart = msg_id + bown_id
    ciphpart = ba + bb + r + ra

    ciph = Cipher(keys[own_id])
    ciphpart = ciph.encrypt(ciphpart)

    return plainpart + ciphpart


def read_session_key(k, ra, bytes):
    # no extra block for ra, padding at end of msg
    ciph = Cipher(k)
    bytesplain = ciph.decrypt(bytes)
    received_ra = bytesplain[0:4]
    ks = bytesplain[4:4 + ciph.key_size // 8]
    if received_ra == ra:
        return ks
    else:
        print("Otway Rees nonce mismatch!")


def generate_session_key_msg(ka, ra, ks):
    # 4 + keysize komplett encrypted
    # 48 byte
    msg_id = int.to_bytes(or_session_key, length=1, byteorder="big")
    ciph = Cipher(ka)
    msg_plain = ra + ks
    padding = b"\00" * ciph.padding_size(len(msg_plain))
    msg_enc = ciph.encrypt(msg_plain + padding)
    return msg_id + msg_enc


def read_session_key_request(bytes):
    halflength = 4 + Cipher.block_size // 8  # assuming encrypted part fits in one block
    a_plaintext = read_sesskey_rq_half(bytes[:halflength])
    b_plaintext = read_sesskey_rq_half(bytes[halflength:])

    ka_a, ka_b, ka_r, ka_rx = read_enc_part(a_plaintext)
    kb_a, kb_b, kb_r, kb_rx = read_enc_part(b_plaintext)

    if ka_a != kb_a:
        print("Otway Rees Session Key Request mismatch: A")
        print("From Client:", ka_a)
        print("From Server:", kb_a)

    if ka_b != kb_b:
        print("Otway Rees Session Key Request mismatch: B")
        print("From Client:", ka_b)
        print("From Server:", kb_b)

    if ka_r != kb_r:
        print("Otway Rees Session Key Request mismatch: Nonce")
        print("From Client:", ka_r)
        print("From Server:", kb_r)

    # A,B,RA,RB, for generating session key message
    return ka_a, ka_b, ka_rx, kb_rx


def read_sesskey_rq_half(bytes):
    # session key requests comes with 2 identities
    # this utility function reads one of them

    id = int.from_bytes(bytes[0:4], byteorder="big")
    ciphertext = bytes[4:4 + Cipher.block_size // 8]
    cipher = Cipher(keys[id])
    plaintext = cipher.decrypt(ciphertext)
    return plaintext


def read_enc_part(bytes):
    # enc part has to be decrypted first
    apos = 0
    bpos = apos + 4
    rpos = bpos + 4
    rxpos = rpos + 4

    a = int.from_bytes(bytes[apos:bpos], byteorder="big")
    b = int.from_bytes(bytes[bpos:rpos], byteorder="big")
    r = bytes[rpos:rxpos]
    rx = bytes[rxpos:]

    return a, b, r, rx


