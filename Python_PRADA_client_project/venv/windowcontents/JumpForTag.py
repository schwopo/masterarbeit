from windowcontents.Content import Content


class JumpForTag(Content):



    def __init__(self, whitespace, rows_given = False, columns_given = False, rows = None, columns = None):
        self.content_id = Content.JUMP_FOR
        self.whitespace = whitespace
        self.jumpRows = rows_given
        self.jumpColumns = columns_given
        if(rows_given):
            self.rows = rows
        if(columns_given):
            self.columns = columns
