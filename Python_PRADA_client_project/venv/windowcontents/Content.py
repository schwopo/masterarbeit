from __future__ import annotations
from abc import ABC

class Content(ABC):

    """
    The content interface is used for all window content classes
    """

    WINDOW          = 0
    SPLIT           = 1
    JUMP_FOR        = 2
    JUMP_TO         = 3
    LINK            = 4
    CONSTANT_DF     = 5
    INPUT_FIELD     = 6
    END_WIN         = 7
    TEXT            = 8


    content_id = None


