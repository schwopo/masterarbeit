from windowcontents.Content import Content


class EndWinTag(Content):

    def __init__(self, n_skips):
        self.content_id = Content.END_WIN
        self.skips = n_skips
