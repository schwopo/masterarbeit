from windowcontents.Content import Content


class TextContent(Content):

    def __init__(self, text_data: bytearray):
        self.content_id = Content.TEXT
        self.textData = text_data
