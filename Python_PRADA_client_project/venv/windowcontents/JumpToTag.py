from windowcontents.Content import Content


class JumpToTag(Content):

    def __init__(self, row, col):
        self.content_id = Content.JUMP_TO
        self.row = row
        self.column = col