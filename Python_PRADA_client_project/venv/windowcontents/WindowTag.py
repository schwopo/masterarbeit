from windowcontents.Content import Content


class WindowTag(Content):

    def __init__(self, kwc, win_id):
        self.content_id = Content.WINDOW
        self.keep_content = kwc
        self.win_id = win_id