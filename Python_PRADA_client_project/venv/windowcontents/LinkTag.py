from windowcontents.Content import Content
from protocolDataTypes.PDT import DataType


class LinkTag(Content):

    def __init__(self,link_id, pdt: DataType):
        self.content_id = Content.LINK
        self.pdt = pdt
        self.link_id = link_id
        self.display_value = pdt.to_string()