from windowcontents.Content import Content
from protocolDataTypes.String import String


class InputField(Content):

    def __init__(self, end_of_line, dfault_text, hide_text, input_field_id, pdt_id, max_inp_size, field_width = None, pdt_string: String = None):
        self.content_id = Content.INPUT_FIELD

        self.end_of_line = end_of_line
        self.dfault_text = dfault_text
        self.hide_text = hide_text

        self.input_field_id = input_field_id
        self.pdt_id = pdt_id
        self.max_inp_size = max_inp_size

        if(not end_of_line):
            self.field_width = field_width

        if(dfault_text):
            self.pdt_string = pdt_string
