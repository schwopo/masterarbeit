from windowcontents.Content import Content
from protocolDataTypes.PDT import DataType
from utils.ByteUtils import getBinString

class ConstantDataField(Content):

    def __init__(self, has_fw, pdt: DataType, field_width = None):
        self.content_id = Content.CONSTANT_DF
        self.has_fw = has_fw
        self.pdt = pdt
        self.display_value = pdt.to_string()
        print("Display Value: " + getBinString(self.pdt.byte_representation))
        if(has_fw):
            self.fielw_width =  field_width