from windowcontents.Content import Content


class SplitTag(Content):

    def __init__(self, split_dir, split_marg, fst_win_id, snd_win_id):
        self.content_id = Content.SPLIT
        self.splitDir = split_dir
        self.splitMargin = split_marg
        self.fstWinID = fst_win_id
        self.sndWinID = snd_win_id
