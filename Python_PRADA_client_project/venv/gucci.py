class GucciHeader:
    header_size = 4 + 1
    def __init__(self, size, padding):
        self.size = size
        self.padding = padding

    def to_bytes(self):
        size_field_size = 4
        padding_size = 1

        size_field = self.size.to_bytes(4, byteorder="big")
        padding_field = self.padding.to_bytes(1, byteorder="big")

        header_bytes = size_field + padding_field
        return header_bytes

    def from_bytes(bytes):
        size_field = bytes[0:4]
        padding_field = bytes[4:5]
        size = int.from_bytes(size_field, byteorder="big", signed=False)
        padding = int.from_bytes(padding_field, byteorder="big", signed=False)
        return GucciHeader(size, padding)

class NoneHeader:
    header_size = 0

    def to_bytes(self):
        return b""

    def from_bytes(bytes):
        return NoneHeader(0, 0)

    def __init__(self, size, padding):
        self.size = size
        self.padding = 0
