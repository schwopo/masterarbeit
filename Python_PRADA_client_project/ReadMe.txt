This is a PyCharm project.
It can be imported into Pycharm, but the python.exe path has to be updated to run.

Rough overview of features missing:
-Add missing protocol data types to byte_ref_from_string method.
 -> Client can not yet encode user input to all protocol data types.

-Updatable Data Fields
-Multimedia Data Fields(Atleast the display of a jpg or sth.)
-Subscribable Data Fields
-Interaction method on how the user can subscribe, how given update rates are displayed
 and last data field values, and where the received values will be saved.

Minor Things:
-Drawing of Boxes is done every time atm (For each leaf window). Change it to be according 
 to the flag in Window Tag ( + not anymore drawn with implicit jump)
- Keeping track of links and input fields in dict when overwritten and deletion.
- Some way to get out of input fields?
-Draw root window with default size, if flag is set.
-Hide user input if Flag set. (Text field content is than realy the replaced characters. 
 Where to save the actual input?)
-Allow or do not allow resizing of window
	- If allowed. Each node only knows its latest update, therefore if it were to be refreshed not all
	  established content can be redrawn. Either save sequence of updates or save each window's complete
	  content after each update. 
-Some more error handling would be good
-Protocol number to 0
-Have a look on endianess for fields again. May be different to specification now.
-Check if communication is done as the lattest specification states.
-Maybe introduce a connection state machine. Could be easier than all these bools.