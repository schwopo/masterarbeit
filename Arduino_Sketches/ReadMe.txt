These folder contains both the PRADA server version with and without an exemplary UI.

Missing features for both:
-Decoding of some protocol data types send from the client via input fields
-Updatable Data Fields
-Multimedia Data Fields
-Subscribable Data Fields
  -> individual updates
  -> publish subscribe pattern (subscribe, unsubscribe and subscription updates)
-Buffering of msg or response if server is still waiting for an ACK,
  -> send_reply method has to do this and has to consider the following.
  -> The server can not send ack if it is waiting, has to be kept in mind. 
-Prior values message.