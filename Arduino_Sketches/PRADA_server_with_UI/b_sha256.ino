#include <SHA256.h>

void sha256(byte *out, byte *in, size_t in_length) {
	SHA256 hashobj = SHA256();
	hashobj.update(in, in_length);
	hashobj.finalize(out, 32);
}