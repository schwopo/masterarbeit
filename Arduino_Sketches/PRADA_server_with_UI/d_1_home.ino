size_t view_home(byte** msg_pointer) {
  //  0
  byte winTag1[window_tag_size];
  uint8_t win_id_1 = 5;
  windowTag(winTag1, true, win_id_1);
  //5
  byte jumpForXY1[jump_for_xy_size];
  jumpForBoth(jumpForXY1, false, 1, 2);
  
  byte menu_item_1[] = "> Home";
  //jump
  byte menu_item_2[] = "Device details  ";

  byte jumpForXY2[jump_for_xy_size];
  jumpForBoth(jumpForXY2, false, 2, 2);
  byte menu_item_4[] = "Impressum  ";


  byte winTag2[window_tag_size];
  uint8_t win_id_2 = 6;
  windowTag(winTag2, false, win_id_2);

  //6
  //jump
  byte content1[] = "Welcome! This is an example sampling device!";
  byte content2[] = "Enjoy this little Demo!";

  byte winTag3[window_tag_size];
  uint8_t win_id_3 = 2;
  windowTag(winTag3, false, win_id_3);

  //2
  byte jumpForTagXY[jump_for_xy_size];
  jumpForBoth(jumpForTagXY, false, 1, 15);

  //Link1
  byte link1_text[] = "<- Previous";
  size_t text1_size = sizeof(link1_text);

  size_t link1_string_size = get_pdt_string_size(text1_size);
  byte link1_string[link1_string_size];
  get_pdt_string(link1_string, link1_text, text1_size);

  size_t link1_size =  link_tag_size(true, link1_string_size);
  byte link_Tag_1[link1_size];
  linkTag(link_Tag_1, true, 0, pdt_string_id, link1_string_size, link1_string);

  byte jumpForTagX[jump_for_xy_size];
  jumpForColumns(jumpForTagX, false, 20);

  //Link2
  byte link2_text[] = "Next ->";
  size_t text2_size = sizeof(link2_text);

  size_t link2_string_size = get_pdt_string_size(text2_size);
  byte link2_string[link2_string_size];
  get_pdt_string(link2_string, link2_text, text2_size);

  size_t link2_size =  link_tag_size(true, link2_string_size);
  byte link_Tag_2[link2_size];
  linkTag(link_Tag_2, true, 1, pdt_string_id, link2_string_size, link2_string);

  byte * messageParts[]       = {ui_update_pf,          winTag1,          jumpForXY1,       menu_item_1,              jumpForXY1,       menu_item_2,              jumpForXY2,
                                menu_item_4,              winTag2,          jumpForXY1,       content1,          jumpForXY1,       content2,            winTag3,          jumpForTagXY,     link_Tag_1, jumpForTagX,      link_Tag_2};
  size_t noOfParts = sizeof(messageParts) / sizeof(messageParts[0]);
  size_t messagePartsLength[] = {sizeof(ui_update_pf),  window_tag_size,  jump_for_xy_size, sizeof(menu_item_1) - 1,  jump_for_xy_size, sizeof(menu_item_2) - 1,  jump_for_xy_size,
                                sizeof(menu_item_4) - 1,  window_tag_size,  jump_for_xy_size, sizeof(content1) - 1,  jump_for_xy_size, sizeof(content2) - 1,  window_tag_size,  jump_for_xy_size, link1_size, jump_for_x_size,  link2_size};

  return build_byte_message(msg_pointer, messageParts, messagePartsLength, noOfParts, true, true, true);
}
