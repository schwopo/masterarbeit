                //0                 1               2               3               
typedef enum {   SRV_STATE_WAITING, SRV_STATE_REC,  SRV_STATE_PROC, NUM_SRV_STATES  } srv_state_t;
                //0               1                 2                     3               
typedef enum {  CON_STATE_NCON,  CON_STATE_CONIDLE, CON_STATE_CON_NEEDACK, CON_STATE_TERM, NUM_CON_STATES } con_state_t;

                //0            1               2                3               4              5                   6
typedef enum {  UI_STATE_NOUI, UI_STATE_INIT, UI_STATE_HOME,  UI_STATE_DINFOS, UI_STATE_TEMPS, UI_STATE_IMPRESSUM, NUM_UI_STATES } ui_state_t;
  typedef enum {LINK_CLICKED, INPUT_FIELD} user_input_t;
  struct data {
    user_input_t input_type;
    uint8_t input_id;
    byte *optional_payload;
  };
  typedef struct data ui_data_t;

typedef struct gucci_header {
  uint32_t size; //host byte order
  uint8_t padding;
} gucci_header;


srv_state_t cur_srv_state;
con_state_t cur_con_state;
ui_state_t cur_ui_state;

void setup() {
   Serial.begin(9600);
   Serial.setTimeout(600000);
   delay(300);

   cur_srv_state = SRV_STATE_WAITING;
   cur_con_state = CON_STATE_NCON;
   cur_ui_state = UI_STATE_NOUI;

   read_id();
   read_shared_key();
}

void loop() {
  cur_srv_state = run_srv_state(cur_srv_state);
} 
