size_t con_accept(byte** msg_pointer){
  byte * messageParts[]       = {init_ds_pf}; //Default size is flaged 1 even if no UI is there to indicate the amount of protocol fields to the client.
  size_t noOfParts = sizeof(messageParts)/sizeof(messageParts[0]);    
  size_t messagePartsLength[] = {sizeof(init_ds_pf)};      
  //sendMessage(messageParts, messagePartsLength, noOfParts, true, true);
  return build_byte_message(msg_pointer, messageParts, messagePartsLength, noOfParts, true, true, true);
}
/*
size_t init_view(byte** msg_pointer){
  
  //UFIXED32
  byte if_tag0_text[] = "UFIXED32";
  size_t if_tag0_text_size = sizeof(if_tag0_text);
  
  size_t if_tag0_pdt_string_size = get_pdt_string_size(if_tag0_text_size);
  byte if_tag0_pdt_string[if_tag0_pdt_string_size];
  get_pdt_string(if_tag0_pdt_string, if_tag0_text, if_tag0_text_size);
  
  size_t if_tag0_size = input_field_tag_size( false, true, 64, if_tag0_pdt_string_size);
  byte input_field_tag0[if_tag0_size];                                                  
  inputFieldTag(input_field_tag0, false, true, false, 0, 9, pdt_ufixed32_id, 64, if_tag0_pdt_string_size, if_tag0_pdt_string);
  
  //JUMP
  byte jumpForXY1[jump_for_xy_size];
  jumpForBoth(jumpForXY1, false, 1, 0);


  //SFIXED32
  byte if_tag1_text[] = "SFIXED32";
  size_t if_tag1_text_size = sizeof(if_tag1_text);
  
  size_t if_tag1_pdt_string_size = get_pdt_string_size(if_tag1_text_size);
  byte if_tag1_pdt_string[if_tag1_pdt_string_size];
  get_pdt_string(if_tag1_pdt_string, if_tag1_text, if_tag1_text_size);
  
  size_t if_tag1_size = input_field_tag_size( false, true, 64, if_tag1_pdt_string_size);
  byte input_field_tag1[if_tag1_size];                                                  
  inputFieldTag(input_field_tag1, false, true, false, 1, 9, pdt_sfixed32_id, 64, if_tag1_pdt_string_size, if_tag1_pdt_string);

  
  //FLOAT
  byte if_tag2_text[] = "FLOAT";
  size_t if_tag2_text_size = sizeof(if_tag2_text);

  size_t if_tag2_pdt_string_size = get_pdt_string_size(if_tag2_text_size);
  byte if_tag2_pdt_string[if_tag2_pdt_string_size];
  get_pdt_string(if_tag2_pdt_string, if_tag2_text, if_tag2_text_size);

  size_t if_tag2_size = input_field_tag_size( false, true, 64, if_tag2_pdt_string_size);
  byte input_field_tag2[if_tag2_size];                                                  
  inputFieldTag(input_field_tag2, false, true, false, 2, 9, pdt_float_id, 64, if_tag2_pdt_string_size, if_tag2_pdt_string);


  //UFIXED64
  byte if_tag3_text[] = "UFIXED64";
  size_t if_tag3_text_size = sizeof(if_tag3_text);
  
  size_t if_tag3_pdt_string_size = get_pdt_string_size(if_tag3_text_size);
  byte if_tag3_pdt_string[if_tag3_pdt_string_size];
  get_pdt_string(if_tag3_pdt_string, if_tag3_text, if_tag3_text_size);
  
  size_t if_tag3_size = input_field_tag_size( false, true, 64, if_tag3_pdt_string_size);
  byte input_field_tag3[if_tag3_size];                                                  
  inputFieldTag(input_field_tag3, false, true, false, 3, 9, pdt_ufixed64_id, 64, if_tag3_pdt_string_size, if_tag3_pdt_string);


  //SFIXED64
  byte if_tag4_text[] = "SFIXED64";
  size_t if_tag4_text_size = sizeof(if_tag4_text);
  
  size_t if_tag4_pdt_string_size = get_pdt_string_size(if_tag4_text_size);
  byte if_tag4_pdt_string[if_tag4_pdt_string_size];
  get_pdt_string(if_tag4_pdt_string, if_tag4_text, if_tag4_text_size);
  
  size_t if_tag4_size = input_field_tag_size( false, true, 64, if_tag4_pdt_string_size);
  byte input_field_tag4[if_tag4_size];                                                  
  inputFieldTag(input_field_tag4, false, true, false, 4, 9, pdt_sfixed64_id, 64, if_tag4_pdt_string_size, if_tag4_pdt_string);
  

  
  byte * messageParts[]       = {init_xy_pf,          input_field_tag0, jumpForXY1,       input_field_tag1, jumpForXY1,       input_field_tag2, jumpForXY1,       input_field_tag3,
                                  jumpForXY1,       input_field_tag4};
  size_t noOfParts = sizeof(messageParts)/sizeof(messageParts[0]);       
  size_t messagePartsLength[] = {sizeof(init_xy_pf),  if_tag0_size,     jump_for_xy_size, if_tag1_size,     jump_for_xy_size, if_tag2_size,     jump_for_xy_size, if_tag3_size,
                                  jump_for_xy_size, if_tag4_size};  
      
  return build_byte_message(msg_pointer, messageParts, messagePartsLength, noOfParts, true, true, true );
}
*/
//UFIXED32
size_t ufixed32_update(byte** msg_pointer, uint32_t value){

  byte winTag1[window_tag_size];
  windowTag(winTag1, true, 0);
  
  byte jumpForXY1[jump_for_xy_size];
  jumpForBoth(jumpForXY1, false, 0, 20);

  //VALUE
  byte pdt_ufixed32[ sizeof_fs_32bit_pdt ];
  fixed_size_pdt_to_byte_array(uint_to_pdt_ufixed32( value ), pdt_ufixed32 );
  
  size_t ufixed32_cdf_size = constant_df_size( false, sizeof_fs_32bit_pdt );
  byte ufixed32_cdf[ ufixed32_cdf_size ];  
  constant_data_field( ufixed32_cdf, false, 9, pdt_ufixed32_id, sizeof_fs_32bit_pdt, pdt_ufixed32 );

  
  byte * messageParts[]       = {ui_update_pf,  winTag1,jumpForXY1, ufixed32_cdf};
  size_t noOfParts = sizeof(messageParts)/sizeof(messageParts[0]);       
  size_t messagePartsLength[] = {sizeof(ui_update_pf), window_tag_size, jump_for_xy_size, ufixed32_cdf_size};  
      
  return build_byte_message(msg_pointer, messageParts, messagePartsLength, noOfParts, true, true, true );
}


//SFIXED32
size_t sfixed32_update(byte** msg_pointer, int32_t value){

  byte winTag1[window_tag_size];
  windowTag(winTag1, true, 0);
  
  byte jumpForXY1[jump_for_xy_size];
  jumpForBoth(jumpForXY1, false, 1, 20);

  //VALUE
  byte pdt_sfixed32[ sizeof_fs_32bit_pdt ];
  fixed_size_pdt_to_byte_array(sint_to_pdt_sfixed32( value ), pdt_sfixed32 );
  
  size_t sfixed32_cdf_size = constant_df_size( false, sizeof_fs_32bit_pdt );
  byte sfixed32_cdf[ sfixed32_cdf_size ];  
  constant_data_field( sfixed32_cdf, false, 9, pdt_sfixed32_id, sizeof_fs_32bit_pdt, pdt_sfixed32 );

  
  byte * messageParts[]       = {ui_update_pf,  winTag1,jumpForXY1, sfixed32_cdf};
  size_t noOfParts = sizeof(messageParts)/sizeof(messageParts[0]);       
  size_t messagePartsLength[] = {sizeof(ui_update_pf), window_tag_size, jump_for_xy_size, sfixed32_cdf_size};  
      
  return build_byte_message(msg_pointer, messageParts, messagePartsLength, noOfParts, true, true, true );
}

//FLOAT
size_t float_update(byte** msg_pointer, float value){

  byte winTag1[window_tag_size];
  windowTag(winTag1, true, 0);
  
  byte jumpForXY1[jump_for_xy_size];
  jumpForBoth(jumpForXY1, false, 2, 20);

  //VALUE
  byte pdt_float[ sizeof_fs_32bit_pdt ];
  fixed_size_pdt_to_byte_array(value, pdt_float );
  
  size_t float_cdf_size = constant_df_size( false, sizeof_fs_32bit_pdt );
  byte float_cdf[ float_cdf_size ];  
  constant_data_field( float_cdf, false, 9, pdt_float_id, sizeof_fs_32bit_pdt, pdt_float );

  
  byte * messageParts[]       = {ui_update_pf,  winTag1,jumpForXY1,  float_cdf};
  size_t noOfParts = sizeof(messageParts)/sizeof(messageParts[0]);       
  size_t messagePartsLength[] = {sizeof(ui_update_pf), window_tag_size, jump_for_xy_size, float_cdf_size};  
      
  return build_byte_message(msg_pointer, messageParts, messagePartsLength, noOfParts, true, true, true );
}

//UFIXED64
size_t ufixed64_update(byte** msg_pointer, uint64_t value){

  byte winTag1[window_tag_size];
  windowTag(winTag1, true, 0);
  
  byte jumpForXY1[jump_for_xy_size];
  jumpForBoth(jumpForXY1, false, 3, 20);

  //VALUE
  byte pdt_ufixed64[ sizeof_fs_64bit_pdt ];
  fixed_size_pdt_to_byte_array(uint_to_pdt_ufixed64( value ), pdt_ufixed64 );
  
  size_t ufixed64_cdf_size = constant_df_size( false, sizeof_fs_64bit_pdt );
  byte ufixed64_cdf[ ufixed64_cdf_size ];  
  constant_data_field( ufixed64_cdf, false, 9, pdt_ufixed64_id, sizeof_fs_64bit_pdt, pdt_ufixed64 );

  
  byte * messageParts[]       = {ui_update_pf,  winTag1,jumpForXY1, ufixed64_cdf};
  size_t noOfParts = sizeof(messageParts)/sizeof(messageParts[0]);       
  size_t messagePartsLength[] = {sizeof(ui_update_pf), window_tag_size, jump_for_xy_size, ufixed64_cdf_size};  
      
  return build_byte_message(msg_pointer, messageParts, messagePartsLength, noOfParts, true, true, true );
}


//SFIXED64
size_t sfixed64_update(byte** msg_pointer, int64_t value){

  byte winTag1[window_tag_size];
  windowTag(winTag1, true, 0);
  
  byte jumpForXY1[jump_for_xy_size];
  jumpForBoth(jumpForXY1, false, 4, 20);

  //VALUE
  byte pdt_sfixed64[ sizeof_fs_64bit_pdt ];
  fixed_size_pdt_to_byte_array(sint_to_pdt_sfixed64( value ), pdt_sfixed64 );
  
  size_t sfixed64_cdf_size = constant_df_size( false, sizeof_fs_64bit_pdt );
  byte sfixed64_cdf[ sfixed64_cdf_size ];  
  constant_data_field( sfixed64_cdf, false, 9, pdt_sfixed64_id, sizeof_fs_64bit_pdt, pdt_sfixed64 );

  
  byte * messageParts[]       = {ui_update_pf,  winTag1,jumpForXY1, sfixed64_cdf};
  size_t noOfParts = sizeof(messageParts)/sizeof(messageParts[0]);       
  size_t messagePartsLength[] = {sizeof(ui_update_pf), window_tag_size, jump_for_xy_size, sfixed64_cdf_size};  
      
  return build_byte_message(msg_pointer, messageParts, messagePartsLength, noOfParts, true, true, true );
}

size_t init_view(byte** msg_pointer){
  //  Init Message
  //  0
  byte splitTag1[split_tag_size];
  splitWindow(splitTag1, false, 12, 1, 2);
        
    //  1 
    byte splitTag2[split_tag_size];
    splitWindow(splitTag2, false, 3, 3, 4);
      
      //  3
      byte jumpForTag1[jump_for_xy_size];
      jumpForBoth(jumpForTag1, false, 1, 25);

      byte headline[] = "Example Sampling Device";
      byte window_end_no_skip[1] = {windowEnd(0)};

      //  4
      byte splitTag3[split_tag_size];
      splitWindow(splitTag3, true, 20, 5, 6);

        //  5
        byte jumpForTag2[jump_for_xy_size];
        jumpForBoth(jumpForTag2, false, 1, 2);
        byte menu_item_1[] = "Home";
        byte menu_item_2[] = "Device details";
        byte menu_item_3[] = "Temperature";
        byte menu_item_4[] = "Impressum";

          //ENDWIN_NO_SKIPS

        //  6
        byte content[] = "Please login below:";

        byte winTag1[window_tag_size];
        uint8_t win_id = 2;
        windowTag(winTag1, false, win_id);

    //  2
    byte links[] = "Login:"; 
    
    byte jumpForTagXY[jump_for_xy_size];
    jumpForBoth(jumpForTagXY, false, 1, 15);  
    /*      
    //Link1 
    byte link1_text[] = "Enter here:";
    size_t text1_size = sizeof(link1_text);
    
    size_t link1_string_size = get_pdt_string_size(text1_size);   
    byte link1_string[link1_string_size];
    get_pdt_string(link1_string, link1_text, text1_size);
    
    size_t link1_size =  link_tag_size(true, link1_string_size);
    byte link_Tag_1[link1_size];  
    linkTag(link_Tag_1, true, 0, pdt_string_id, link1_string_size, link1_string);    
    */
     byte if_tag0_text[] = "Password";
    size_t if_tag0_text_size = sizeof(if_tag0_text);
  
    size_t if_tag0_pdt_string_size = get_pdt_string_size(if_tag0_text_size);
    byte if_tag0_pdt_string[if_tag0_pdt_string_size];
    get_pdt_string(if_tag0_pdt_string, if_tag0_text, if_tag0_text_size);
  
    size_t if_tag0_size = input_field_tag_size( false, true, 64, if_tag0_pdt_string_size);
    byte input_field_tag0[if_tag0_size];                                                  
    inputFieldTag(input_field_tag0, false, true, false, 0, 9, pdt_ufixed32_id, 64, if_tag0_pdt_string_size, if_tag0_pdt_string);
    
  byte * messageParts[]       = {init_xy_pf,         splitTag1,      splitTag2,      jumpForTag1,      headline,           window_end_no_skip, splitTag3,       jumpForTag2,      menu_item_1,           
                                jumpForTag2,       menu_item_2,           jumpForTag2,      menu_item_3,            jumpForTag2,       menu_item_4,           window_end_no_skip, jumpForTag2,
                                content,              winTag1,          links,            jumpForTagXY,     input_field_tag0};
  size_t noOfParts = sizeof(messageParts)/sizeof(messageParts[0]);       
  size_t messagePartsLength[] = {sizeof(init_xy_pf), split_tag_size, split_tag_size, jump_for_xy_size, sizeof(headline)-1, window_end_size,    split_tag_size,  jump_for_xy_size, sizeof(menu_item_1)-1, 
                                jump_for_xy_size,  sizeof(menu_item_2)-1, jump_for_xy_size, sizeof(menu_item_3)-1,  jump_for_xy_size,  sizeof(menu_item_4)-1, window_end_size,    jump_for_xy_size,
                                 sizeof(content)-1,   window_tag_size,  sizeof(links)-1,  jump_for_xy_size, if_tag0_size};        
  return build_byte_message(msg_pointer, messageParts, messagePartsLength, noOfParts, true, true, true );
}
