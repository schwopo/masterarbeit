size_t view_impressum(byte** msg_pointer) {
    //  0
  byte winTag1[window_tag_size];
  uint8_t win_id_1 = 5;
  windowTag(winTag1, true, win_id_1);
  //5
  byte jumpForXY1[jump_for_xy_size];
  jumpForBoth(jumpForXY1, false, 1, 2);
  
  byte menu_item_1[] = "Home  ";

  byte jumpForXY2[jump_for_xy_size];
  jumpForBoth(jumpForXY2, false, 2, 2);
 
  byte menu_item_3[] = "Temperature  ";
  //jump
  byte menu_item_4[] = "> Impressum ";


  byte winTag2[window_tag_size];
  uint8_t win_id_2 = 6;
  windowTag(winTag2, false, win_id_2);

  //6
  //jump
  byte content1[] = "Details on the guy who made this:";
  byte content2[] = "Matthias Schaffeld";
  byte content3[] = "Hopefully soon a M.Sc";
  
  
  byte * messageParts[]       = {ui_update_pf,          winTag1,          jumpForXY1,       menu_item_1,              jumpForXY2,       menu_item_3,              jumpForXY1,
                                menu_item_4,              winTag2,          jumpForXY1,       content1,             jumpForXY1,       content2,             jumpForXY1,       content3};
  size_t noOfParts = sizeof(messageParts) / sizeof(messageParts[0]);
  size_t messagePartsLength[] = {sizeof(ui_update_pf),  window_tag_size,  jump_for_xy_size, sizeof(menu_item_1) - 1,  jump_for_xy_size, sizeof(menu_item_3) - 1,  jump_for_xy_size,
                                sizeof(menu_item_4) - 1,  window_tag_size,  jump_for_xy_size, sizeof(content1) - 1, jump_for_xy_size, sizeof(content2) - 1, jump_for_xy_size, sizeof(content3) - 1};

  return build_byte_message(msg_pointer, messageParts, messagePartsLength, noOfParts, true, true, true);
}
