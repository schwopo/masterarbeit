#include <EEPROM.h>

// eeprom layout:
// 0-4: id
// 4-36: shared_key

void read_shared_key() {
	read_from_eeprom(gucci_shared_key, 4, gucci_key_size);
}

void read_id() {
	read_from_eeprom(or_own_id, 0, sizeof or_own_id);
}

void write_shared_key() {
	write_to_eeprom(gucci_shared_key, 4, gucci_key_size);
}

void write_id() {
	write_to_eeprom(or_own_id, 0, sizeof or_own_id);
}

void write_to_eeprom(byte *address, int address_in_rom, size_t size) {
	for (int i = 0; i<size; i++) {
		EEPROM.write(address_in_rom + i, address[i]);
	}
}

void read_from_eeprom(byte *address, int address_in_rom, size_t size) {
	for (int i = 0; i<size; i++) {
		address[i] = EEPROM.read(address_in_rom + i);
	}
}
