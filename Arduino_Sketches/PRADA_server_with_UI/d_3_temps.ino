size_t view_temps(byte** msg_pointer){
    //  0
  byte winTag1[window_tag_size];
  uint8_t win_id_1 = 5;
  windowTag(winTag1, true, win_id_1);
  //5
  
  byte jumpForXY2[jump_for_xy_size];
  jumpForBoth(jumpForXY2, false, 2, 2);
  
  byte menu_item_2[] = "Device details  ";
  
  byte jumpForXY1[jump_for_xy_size];
  jumpForBoth(jumpForXY1, false, 1, 2);
  
  byte menu_item_3[] = "> Temperature";
  //jump
  byte menu_item_4[] = "Impressum  ";


  byte winTag2[window_tag_size];
  uint8_t win_id_2 = 6;
  windowTag(winTag2, false, win_id_2);

  //6
  //jump
  byte content1[] = "Some temperature samplings:";
  
  byte content2[] = "Stove:";

  float value0 = 0;
  byte pdt_float0[ sizeof_fs_32bit_pdt ];
  fixed_size_pdt_to_byte_array(value0, pdt_float0 );
  
  size_t float_cdf_size = constant_df_size( false, sizeof_fs_32bit_pdt );
  byte float_cdf0[ float_cdf_size ];  
  constant_data_field( float_cdf0, false, 9, pdt_float_id, sizeof_fs_32bit_pdt, pdt_float0 );
  
  byte jumpForX1[jump_for_x_size];
  jumpForColumns(jumpForX1, false, 5);
  
  byte content3[] = "Lightbulb: ";

  float value1 = 200;
  byte pdt_float1[ sizeof_fs_32bit_pdt ];
  fixed_size_pdt_to_byte_array(value1, pdt_float1 );
  
  byte float_cdf1[ float_cdf_size ];  
  constant_data_field( float_cdf1, false, 9, pdt_float_id, sizeof_fs_32bit_pdt, pdt_float1 );
  
  byte jumpForX2[jump_for_x_size];
  jumpForColumns(jumpForX2, false, 6);
  
  byte content4[] = "Oven:";

  float value2 = 180;
  byte pdt_float2[ sizeof_fs_32bit_pdt ];
  fixed_size_pdt_to_byte_array(value2, pdt_float2 );
  
  byte float_cdf2[ float_cdf_size ];  
  constant_data_field( float_cdf2, false, 9, pdt_float_id, sizeof_fs_32bit_pdt, pdt_float2 );
  
  byte content5[] = " °C";
  
  
  byte * messageParts[]       = {ui_update_pf,          winTag1,          jumpForXY2,       menu_item_2,              jumpForXY1,       menu_item_3,              jumpForXY1,
                                menu_item_4,              winTag2,          jumpForXY1,       content1,             jumpForXY2,       content2,             jumpForX1,        float_cdf0,       content5,             jumpForXY1,
                                content3,             float_cdf1,     content5,             jumpForXY1,       content4,             jumpForX2,        float_cdf2,     content5};
  size_t noOfParts = sizeof(messageParts) / sizeof(messageParts[0]);
  size_t messagePartsLength[] = {sizeof(ui_update_pf),  window_tag_size,  jump_for_xy_size, sizeof(menu_item_2) - 1,  jump_for_xy_size, sizeof(menu_item_3) - 1,  jump_for_xy_size,
                                sizeof(menu_item_4) - 1,  window_tag_size,  jump_for_xy_size, sizeof(content1) - 1, jump_for_xy_size, sizeof(content2) - 1, jump_for_x_size,  float_cdf_size,   sizeof(content5) - 1, jump_for_xy_size,
                                sizeof(content3) - 1, float_cdf_size, sizeof(content5) - 1, jump_for_xy_size, sizeof(content4) - 1, jump_for_x_size,  float_cdf_size, sizeof(content5) - 1};

  return build_byte_message(msg_pointer, messageParts, messagePartsLength, noOfParts, true, true, true);
}
