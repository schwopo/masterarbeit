size_t noop_ack(byte** msg_pointer){
  byte *msg_parts[] = {noop_msg};
  size_t part_sizes[] = {sizeof(noop_msg)};
  return build_byte_message(msg_pointer, msg_parts,part_sizes, 1, false, true, false);
}

size_t heartbeat(byte** msg_pointer){
  byte *msg_parts[] = {heartbeat_msg};
  size_t part_sizes[] = {sizeof( heartbeat_msg)};
  return build_byte_message(msg_pointer, msg_parts,part_sizes, 1, true, false, false);
}

void get_seq_no(uint16_t *seq_no, byte *received_msg){
  *seq_no |= received_msg[ 1 ];
  *seq_no <<= 8;
  *seq_no |= received_msg[ 2 ];
}

boolean get_ack_no(uint8_t msg_type, uint16_t *ack_no, byte *received_msg){
    bool msg_includes_ack = bitRead( received_msg[ 0 ], 3 );
    uint8_t offset = (msg_type == msg_id_noop) ?  0 : 2;
    if( msg_includes_ack ){
      *ack_no |= received_msg[ 1 + offset ];
      *ack_no <<= 8;
      *ack_no |= received_msg[ 2 + offset ];
    }
    return msg_includes_ack;
}

uint8_t get_msg_size( uint8_t msg_type, byte current_byte ){
  uint8_t msg_size = 0;
  switch ( msg_type ) { 
    case msg_id_con_init :
      msg_size = msg_size_con_init;
      if( bitRead( current_byte, 2 )) {
        msg_size += 2;
      }
      break;
    case msg_id_link_clicked  :
      msg_size = msg_size_link_clicked;
      break;
    case msg_id_user_input    :
      msg_size = msg_size_client_input;
      break;
    case msg_id_subscribe     :
      msg_size = msg_size_subscribe;
      if( bitRead( current_byte, 1 )) {
        msg_size += 1;
      }
      if( bitRead( current_byte, 2 )) {
        msg_size += 1;
      }
      break;
    case msg_id_change_ur     :
      msg_size = msg_size_change_ur;
      break;
    case msg_id_unsubscribe   :
      msg_size = msg_size_unsubscribe;
      break;
    case msg_id_last_x_val    :
      msg_size = msg_size_last_x_val;
      break;
    case msg_id_noop          :
      msg_size = msg_size_noop;
      break;
    case  msg_id_con_term     :
      msg_size = msg_size_con_term;
      break;
    case msg_id_error         :
      msg_size = msg_size_error;
      break; 
  }
  if( msg_type != msg_id_con_init){
    if( bitRead( current_byte, 3 )) {      
        msg_size += 2;     
      }
  }
  return msg_size;
}

size_t build_byte_message(byte** msg_pointer, byte * msg_parts[], size_t part_sizes[], size_t n_parts, bool seq_no, bool ack, bool payload ){
  //  Determine total msg size:
  size_t msg_size = 0;
  
    //  payload_size and payload_size_size
    uint16_t payload_size = 0;
    byte payload_varint[ 10 ];
    size_t payload_varint_size = 0;  //  max. 10
    
    if(payload){
      for ( int i = 1; i < n_parts; ++i ) {             // First element of messageParts are always protocol fields -> i = 1
        payload_size += part_sizes[i];
      }
      payload_varint_size = uint_to_varint(payload_size, payload_varint);
    }
         
    // no_of_pfs + payload_varint_size + seq_no and ack_no  
    msg_size += part_sizes[ 0 ] + ( seq_no ? 2 : 0 ) + ( ack ? 2 : 0 ) + payload_varint_size + payload_size;

  *msg_pointer = ( byte* )malloc( msg_size * sizeof( byte ) );
  byte* msg = *msg_pointer;
  size_t msg_index = 0;

  //  First protocol field
  msg[ msg_index++ ] = msg_parts[ 0 ][ 0 ];

  //  Seq no.:
  if( seq_no ){
    byte *seq_no_begin =  ( byte* )( &server_sequence_no );
    msg[ msg_index++ ] = seq_no_begin[ 1 ];
    msg[ msg_index++ ] = seq_no_begin[ 0 ]; 
  }
  //  Ack no.:
  if( ack ){
      msg[ 0 ] |= 8;
      byte *expected_client_sequence_no_begin =  ( byte* )( &expected_client_sequence_no );
      msg[ msg_index++ ] = expected_client_sequence_no_begin[ 1 ];
      msg[ msg_index++ ] = expected_client_sequence_no_begin[ 0 ]; 
  }

  //  Rest of protocol fields
  for( int i = 1; i < part_sizes[ 0 ]; ++i ){
    msg[ msg_index++ ] = msg_parts[ 0 ][ i ];
  }
  
  if( payload ){
    //  Last protocol field if payload is there
    for ( int i = 0; i < payload_varint_size; ++i ) {
       msg[ msg_index++ ] = payload_varint[ i ];
    }
    //  Actual payload
    for ( int i = 1; i < n_parts; ++i ){ //  first one was protocol fields          
      for ( int j = 0; j < part_sizes[i]; ++j ){
         msg[ msg_index++ ] = msg_parts[ i ][ j ];
      }
    }
  }
  return msg_size;  
}
