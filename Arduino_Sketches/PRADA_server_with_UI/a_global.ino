//Message and connection constants:
        //  Connection Constants
    static const uint8_t srv_protocol_version = 1;
    static const unsigned long server_timeout_in_s = 64;
    static const unsigned long heartbeat_in_ms = ( 1000 * server_timeout_in_s) / 2;
    static const unsigned long msg_timeout_in_ms = heartbeat_in_ms / 4;
    static const unsigned long term_timeout_in_ms = 4000;
    static const uint8_t max_tries = 4;
    static const uint8_t max_window_y   = 15;
    static const uint8_t max_window_x   = 75;  
    
    //  Messages and messageparts without ack field set.
    byte init_xy_pf[4]    = {9,  server_timeout_in_s, max_window_y, max_window_x};
    byte init_ds_pf[2]    = {13, server_timeout_in_s};
    byte ui_update_pf[1]  = {48};
    byte noop_msg[1]      = {168};
    byte heartbeat_msg[1] = {176};
    byte con_term_msg[1]  = {192};
    byte error_msg[1]     = {208};
     
    byte no_con_error_msg[5]     = {216,0,0,0,1};      
      
    //  Message ids
    static const uint8_t msg_id_con_init      = 0;
    static const uint8_t msg_id_link_clicked  = 1;
    static const uint8_t msg_id_user_input    = 2;  //  3: ui update
    static const uint8_t msg_id_subscribe     = 4;  //  5: data field update  
    static const uint8_t msg_id_change_ur     = 6;
    static const uint8_t msg_id_unsubscribe   = 7;
    static const uint8_t msg_id_last_x_val    = 8;  //  9:  srv confirm
    static const uint8_t msg_id_noop          = 10; //  11: Heartbeat
    static const uint8_t msg_id_con_term      = 12;
    static const uint8_t msg_id_error         = 13;
    
    // Client message sizes excluding payload. 2 bytes for seq. no. Bytes for Ack not included
    static const uint8_t msg_size_con_init        = 3;
    static const uint8_t msg_size_link_clicked    = 4;
    static const uint8_t msg_size_client_input    = 4;

    static const uint8_t msg_size_subscribe       = 4;
    static const uint8_t msg_size_change_ur       = 5;
    static const uint8_t msg_size_unsubscribe     = 4;
    static const uint8_t msg_size_last_x_val      = 5;
    
    static const uint8_t msg_size_noop            = 1;

    static const uint8_t msg_size_con_term        = 3;
    static const uint8_t msg_size_error           = 3;

    static const uint8_t max_client_msg_size      = 8;  //  max message size of messages from client to srv (excluding the user input message)
    
//  UI constants
     
  //  Tag byte size constants
  static const uint8_t window_tag_size   = 2;
  static const uint8_t split_tag_size    = 4;
  static const uint8_t jump_for_x_size   = 2;
  static const uint8_t jump_for_y_size   = 2;
  static const uint8_t jump_for_xy_size  = 3;
  static const uint8_t jump_to_size      = 3; 
  static const uint8_t window_end_size   = 1;

  //  Data type byte size constants:
  static const size_t sizeof_fs_32bit_pdt   = 4;
  static const size_t sizeof_fs_64bit_pdt   = 8;
  static const size_t sizeof_bool_pdt       = 1;

  // Link-IDs
  static const uint8_t link_prev   = 0;
  static const uint8_t link_next   = 1;

//  Data type id constants
  //  Fixed Length 32 Bit
  static const uint8_t pdt_ufixed32_id = 0;
  static const uint8_t pdt_sfixed32_id = 1;
  static const uint8_t pdt_float_id    = 2;
  //  Fixed Length 64 Bit
  static const uint8_t pdt_ufixed64_id = 64;
  static const uint8_t pdt_sfixed64_id = 65;
  static const uint8_t pdt_double_id   = 66;
  //  Varint encoded
  static const uint8_t pdt_boolean_id  = 128;
  static const uint8_t pdt_uint32_id   = 129;
  static const uint8_t pdt_uint64_id   = 130;
  static const uint8_t pdt_sint32_id   = 131;
  static const uint8_t pdt_sint64_id   = 132;
  //  
  static const uint8_t pdt_string_id   = 192;
  static const uint8_t pdt_bytes_id    = 193;

  //  ID Mappings
  static const uint8_t input_field_id_to_pdt[12] = { pdt_ufixed32_id, pdt_sfixed32_id, pdt_float_id, pdt_ufixed64_id, pdt_sfixed64_id } ;

//  Varint constants:
  static const uint64_t varint_one_byte_max_value   = 127;
  static const uint64_t varint_two_byte_max_value   = 16383;
  static const uint64_t varint_three_byte_max_value = 2097151;
  static const uint64_t varint_four_byte_max_value  = 268435455;
  static const uint64_t varint_five_byte_max_value  = 34359738367;
  static const uint64_t varint_six_byte_max_value   = 4398046511103;
  static const uint64_t varint_seven_byte_max_value = 562949953421311;
  static const uint64_t varint_eight_byte_max_value = 72057594037927935;
  static const uint64_t varint_nine_byte_max_value  = 9223372036854775807;
  static const uint64_t varint_64_bit_max_value     = 18446744073709551615;

// gucci
const int gucci_max_msg_size = 80; //public key with id for server
const int gucci_max_server_msg_size = 200;

const int gucci_size_field_size = 4;
const int gucci_padding_size = 1;
const int gucci_header_size = gucci_size_field_size + gucci_padding_size;
const int gucci_block_size = 16; //byte
const int gucci_key_size = 32; //byte
extern uint8_t gucci_session_key[];
extern uint8_t gucci_shared_key[];

//otway rees and other message codes
extern uint32_t or_own_id;

const byte coupling_static_pubk_request = 1;
const byte coupling_static_pubk = 2;
const byte coupling_ephemeral_pubk_request = 3;
const byte coupling_ephemeral_pubk = 4;

const byte or_connection_request = 10;
const byte or_session_key_request = 20;
const byte or_session_key = 30;

//diffie hellman

extern uint8_t ecdhe_secret[];
extern uint8_t ecdhe_public[];
extern uint8_t ecdhe_private[];

extern uint8_t hard_public[];
extern uint8_t hard_private[];

const int ecdhe_public_key_size = 64;
const int ecdhe_public_key_msg_size = 64;
const int ecdhe_secret_size = 32;