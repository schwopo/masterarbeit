//typedef enum { UI_STATE_NOUI, UI_STATE_INIT, UI_STATE_HOME,  UI_STATE_DINFOS, UI_STATE_TEMPS, UI_STATE_IMPRESSUM, NUM_UI_STATES } ui_state_t;

ui_state_t do_ui_state_noui(ui_data_t ui_data) {
  return UI_STATE_NOUI;
}

ui_state_t do_ui_state_init(ui_data_t ui_data) {
  ui_state_t new_ui_state = UI_STATE_INIT;
  if (ui_data.input_type == LINK_CLICKED) {
    if (ui_data.input_id == 0) {
      new_ui_state = UI_STATE_HOME;
    }
  }
  return new_ui_state;
}

ui_state_t do_ui_state_home(ui_data_t ui_data) {
  ui_state_t new_ui_state = UI_STATE_HOME;
  if (ui_data.input_type == LINK_CLICKED) {
    if (ui_data.input_id == 0) {
      new_ui_state = UI_STATE_IMPRESSUM;
    } else if (ui_data.input_id == 1) {
      new_ui_state = UI_STATE_DINFOS;
    }
  }
  return new_ui_state;
}

ui_state_t do_ui_state_dinfos(ui_data_t ui_data) {
  ui_state_t new_ui_state = UI_STATE_DINFOS;
  if (ui_data.input_type == LINK_CLICKED) {
    if (ui_data.input_id == 0) {
      new_ui_state = UI_STATE_HOME;
    } else if (ui_data.input_id == 1) {
      new_ui_state = UI_STATE_TEMPS;
    }
  }

  return new_ui_state;
}

ui_state_t do_ui_state_temps(ui_data_t ui_data) {
  ui_state_t new_ui_state = UI_STATE_TEMPS;
  if (ui_data.input_type == LINK_CLICKED) {
    if (ui_data.input_id == 0) {
      new_ui_state = UI_STATE_DINFOS;
    } else if (ui_data.input_id == 1) {
      new_ui_state = UI_STATE_IMPRESSUM;
    }
  }

  return new_ui_state;
}

ui_state_t do_ui_state_impressum(ui_data_t ui_data) {
  ui_state_t new_ui_state = UI_STATE_IMPRESSUM;
  if (ui_data.input_type == LINK_CLICKED) {
    if (ui_data.input_id == 0) {
      new_ui_state = UI_STATE_TEMPS;
    } else if (ui_data.input_id == 1) {
      new_ui_state = UI_STATE_HOME;
    }
  }

  return new_ui_state;
}
