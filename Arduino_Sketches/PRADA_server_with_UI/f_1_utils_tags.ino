///// format message parts
//  Array needs to be 2 bytes long
void windowTag(byte windowTag[], bool kwc, uint8_t winID) {
  windowTag[0] = 128;     // 10 000 0 00
  if (kwc) {
    windowTag[0] |= 4;   // 10 000 1 00
  }
  /*
    if(border){
     windowTag[0] |= 4;   // 10 000 x 10
    }
  */
  windowTag[1] = winID;
}

// Array needs to be 4 bytes long
void splitWindow(byte splitTag[], bool vertical, uint8_t splitAmount, uint8_t firstWindowID, uint8_t sndWindowID) {
  if (vertical) {
    splitTag[0] = 140;  // 10 001 1 00
  } else {
    splitTag[0] = 136;  // 10 001 0 00
  }
  splitTag[1] = splitAmount;
  splitTag[2] = firstWindowID;
  splitTag[3] = sndWindowID;
}

// Array needs to be 2 bytes long
void jumpForRows(byte jumpForTag[], bool whitespace, uint8_t rows) {
  if (whitespace) {
    jumpForTag[0] = 150;  // 10 010 1 1 0
  } else {
    jumpForTag[0] = 146;  // 10 010 0 1 0
  }
  jumpForTag[1] = rows;
}

// Array needs to be 2 bytes long
void jumpForColumns(byte jumpForTag[], bool whitespace, uint8_t columns) {
  if (whitespace) {
    jumpForTag[0] = 149;  // 10 010 1 0 1
  } else {
    jumpForTag[0] = 145;  // 10 010 0 0 1
  }
  jumpForTag[1] = columns;
}

// Array needs to be 3 bytes long
void jumpForBoth(byte jumpForTag[], bool whitespace, uint8_t rows, uint8_t columns) {
  if (whitespace) {
    jumpForTag[0] = 151;  // 10 010 1 1 1
  } else {
    jumpForTag[0] = 147;  // 10 010 0 1 1
  }
  jumpForTag[1] = rows;
  jumpForTag[2] = columns;
}

// Array needs to be 3 bytes long
void jumpToTag(byte jumpToTag[], uint8_t rows, uint8_t columns) {
  jumpToTag[0] = 152;  // 10 011 000
  jumpToTag[1] = rows;
  jumpToTag[2] = columns;
}

// linkTag has to be valueLength + 2 [+ 1] long. optional data type field
void linkTag(byte linkTag[], bool dfault, uint8_t linkID, uint8_t data_type_id, size_t pdt_size, byte pdt[]) {
  //Set protocol fields:
  //10 100 1 00  : 10 100 0 00
  linkTag[0] = dfault ? 164 : 160;
  linkTag[1] = linkID;

  uint8_t valueIndex = 2;
  if (!dfault) {
    linkTag[valueIndex++] = data_type_id;
  }

  for (int j = 0; j < pdt_size; j++) {
    linkTag[valueIndex + j] = pdt[j];
  }
}

size_t link_tag_size(bool dfault, size_t pdt_size) {
  return pdt_size + ( dfault ? 2 : 3 );
}

void constant_data_field( byte const_df[], bool fw, uint8_t field_width, uint8_t data_type_id, size_t pdt_size, byte pdt[]) {
  const_df[0] = fw ? 169 : 168;
  uint8_t valueIndex = 1;
  
  if(fw){
    const_df[valueIndex++] = field_width;
  }
  
  const_df[valueIndex++] = data_type_id;
  
  for (int j = 0; j < pdt_size; j++) {
   const_df[valueIndex + j] = pdt[j];
  }
}

size_t constant_df_size(bool fw, size_t pdt_size){
  return pdt_size + (fw ? 3 : 2 );
}

template<typename uint>
void inputFieldTag(byte input_field_tag[], bool end_of_line, bool default_text, bool hide_text, uint8_t input_id, uint8_t field_width, uint8_t dataType, uint max_inp_len, size_t string_size, byte string[]) {
  
  size_t index = 0;
  input_field_tag[index++] = 176; //10 110 0 0 0
  if (hide_text) {
    input_field_tag[0] |= 1;
  }

  input_field_tag[index++] = input_id;

  if (!end_of_line) {
    input_field_tag[index++] = field_width;
  }else{
    input_field_tag[0] |= 4;
  }

  input_field_tag[index++] = dataType;

  uint8_t varint[10];
  size_t varint_size = uint_to_varint(max_inp_len, varint);

  for (int i = 0; i < varint_size; i++) {
    input_field_tag[index++] = varint[i];
  }

  if (default_text) {
    input_field_tag[0] |= 2;
    for (int j = 0; j < string_size; j++) {
      input_field_tag[index++] = string[j];
    }
  }
}

template<typename uint>
size_t input_field_tag_size( bool end_of_line, bool default_text, uint max_inp_len, size_t pdt_string_size) {
  size_t tag_size = (end_of_line ? 3 : 4) +  uint_varint_size(max_inp_len);
  return default_text ? tag_size + pdt_string_size  : tag_size;
}

uint8_t windowEnd(uint8_t skips) {
  uint8_t windowEnd = 184;
  if (skips < 8) {
    windowEnd |= skips;
  }
  return windowEnd;
}
