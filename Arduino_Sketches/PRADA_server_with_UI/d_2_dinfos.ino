size_t view_dinfos(byte** msg_pointer) {
   //  0
  byte winTag1[window_tag_size];
  uint8_t win_id_1 = 5;
  windowTag(winTag1, true, win_id_1);
  //5
  byte jumpForXY1[jump_for_xy_size];
  jumpForBoth(jumpForXY1, false, 1, 2);
  
  byte menu_item_1[] = "Home  ";
  //jump
  byte menu_item_2[] = "> Device details";
  //jump
  byte menu_item_3[] = "Temperature  ";

  byte winTag2[window_tag_size];
  uint8_t win_id_2 = 6;
  windowTag(winTag2, false, win_id_2);

  //6
  //jump
  byte content1[] = "Arduino Mega 2560:";
  
  byte jumpForXY2[jump_for_xy_size];
  jumpForBoth(jumpForXY2, false, 2, 17);
  
  byte content2[] = "Used:  Total:";
  
  byte jumpForXY3[jump_for_xy_size];
  jumpForBoth(jumpForXY3, false, 1, 2);
  
  byte content3[] = "Flash Memory:  ";
  //Value 1
  byte pdt_ufixed32_used_fm[ sizeof_fs_32bit_pdt ];
  fixed_size_pdt_to_byte_array(uint_to_pdt_ufixed32( 12094 ), pdt_ufixed32_used_fm);
  
  size_t ufixed32_cdf_size = constant_df_size( false, sizeof_fs_32bit_pdt );
  byte ufixed32_cdf_used_fm[ ufixed32_cdf_size ];  
  constant_data_field( ufixed32_cdf_used_fm, false, 9, pdt_ufixed32_id, sizeof_fs_32bit_pdt, pdt_ufixed32_used_fm );
   
  //Jump
  byte jumpForX1[jump_for_x_size];
  jumpForColumns(jumpForX1, false, 2);
  
  //Value 2
    byte pdt_ufixed64_total_fm[ sizeof_fs_64bit_pdt ];
  fixed_size_pdt_to_byte_array(uint_to_pdt_ufixed64( 253952 ), pdt_ufixed64_total_fm);
  
  size_t ufixed64_cdf_size = constant_df_size( false, sizeof_fs_64bit_pdt );
  byte ufixed64_cdf_total_fm[ ufixed64_cdf_size ];  
  constant_data_field( ufixed64_cdf_total_fm, false, 9, pdt_ufixed64_id, sizeof_fs_64bit_pdt, pdt_ufixed64_total_fm );

  //Next Line
  byte jumpForXY4[jump_for_xy_size];
  jumpForBoth(jumpForXY4, false, 1, 10);
  
  byte content4[] = "SRAM:  ";
  //Value 3
  byte pdt_ufixed32_used_fm2[ sizeof_fs_32bit_pdt ];
  fixed_size_pdt_to_byte_array(uint_to_pdt_ufixed32( 840 ), pdt_ufixed32_used_fm2);
  
  byte ufixed32_cdf_used_fm2[ ufixed32_cdf_size ];  
  constant_data_field( ufixed32_cdf_used_fm2, false, 9, pdt_ufixed32_id, sizeof_fs_32bit_pdt, pdt_ufixed32_used_fm2 );
   
  //Jump
  byte jumpForX2[jump_for_x_size];
  jumpForColumns(jumpForX2, false, 4);
  
  //Value 4
  byte pdt_ufixed32_total_fm2[ sizeof_fs_32bit_pdt ];
  fixed_size_pdt_to_byte_array(uint_to_pdt_ufixed32( 8192 ), pdt_ufixed32_total_fm2);
  
  byte ufixed32_cdf_total_fm2[ ufixed32_cdf_size ];  
  constant_data_field( ufixed32_cdf_total_fm2, false, 9, pdt_ufixed32_id, sizeof_fs_32bit_pdt, pdt_ufixed32_total_fm2 );
  
  
  byte * messageParts[]       = {ui_update_pf,          winTag1,          jumpForXY1,       menu_item_1,              jumpForXY1,       menu_item_2,              jumpForXY1,
                                menu_item_3,              winTag2,          jumpForXY1,       content1,             jumpForXY2,       content2,              jumpForXY3,       content3,
                                ufixed32_cdf_used_fm, jumpForX1,        ufixed64_cdf_total_fm,   jumpForXY4,       content4,              ufixed32_cdf_used_fm2, jumpForX2,         ufixed32_cdf_total_fm2};
  size_t noOfParts = sizeof(messageParts) / sizeof(messageParts[0]);
  size_t messagePartsLength[] = {sizeof(ui_update_pf),  window_tag_size,  jump_for_xy_size, sizeof(menu_item_1) - 1,  jump_for_xy_size, sizeof(menu_item_2) - 1,  jump_for_xy_size,
                                sizeof(menu_item_3) - 1,  window_tag_size,  jump_for_xy_size, sizeof(content1) - 1,  jump_for_xy_size, sizeof(content2) - 1, jump_for_xy_size, sizeof(content3) - 1,
                                ufixed32_cdf_size,    jump_for_x_size,  ufixed64_cdf_size,       jump_for_xy_size,  sizeof(content4) - 1, ufixed32_cdf_size,      jump_for_x_size,  ufixed32_cdf_size};

  return build_byte_message(msg_pointer, messageParts, messagePartsLength, noOfParts, true, true, true);
}
