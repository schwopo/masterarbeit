void read_connection_request(byte *msg, uint32_t *out_a, uint32_t *out_b, uint32_t *out_r, byte **out_enc_msg){
	uint32_t *values = (uint32_t *) msg;
	*out_a = ntohl(values[0]);
	*out_b = ntohl(values[1]);
	*out_r = ntohl(values[2]);
	*out_enc_msg = msg + 12;
}

void generate_session_key_request(byte *msg, uint32_t a, uint32_t b,uint32_t r, uint32_t rb, byte* a_cipher_part) {
	//1 + 40 byte 
	msg[0] = or_session_key_request;
	generate_session_key_request_first_half(msg + 1, a, a_cipher_part);
	generate_session_key_request_second_half(msg + 20 + 1, a, b, r, rb);
}

void generate_session_key_request_first_half(byte *msg, uint32_t a, byte* a_cipher_part) {
	//20 byte
	a = htonl(a);

	//plain
	memcpy(msg, &a, 4);

	//cipher
	memcpy(msg + 4, a_cipher_part, 16);
}

void generate_session_key_request_second_half(byte *msg, uint32_t a, uint32_t b, uint32_t r, uint32_t rb) {
	//20 byte
	a = htonl(a);
	b = htonl(b);
	r = htonl(r);
	rb = htonl(rb);

	//plain
	memcpy(msg,      &b,  4);

	//cipher
	memcpy(msg + 4 , &a,  4);
	memcpy(msg + 8 , &b,  4);
	memcpy(msg + 12, &r,  4);
	memcpy(msg + 16, &rb, 4);

	gucci_encrypt(msg + 4, 16, gucci_shared_key);
}

void read_session_key(byte *msg, byte *session_key, uint32_t ra) {
	//decrypts msg inplace
	//assume 3 blocks
	gucci_decrypt(msg, 3 * gucci_block_size, gucci_shared_key);
	uint32_t rra = ntohl(*(uint32_t *) msg);
	if(ra != rra) {
		Serial.write("RA_MISMATCH");
	}
	memcpy(session_key, msg + 4, gucci_key_size);
}