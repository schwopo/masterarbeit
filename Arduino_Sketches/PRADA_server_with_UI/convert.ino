uint32_t ntohl(uint32_t x) {
	uint32_t y;
	byte *b = (byte *) &x;
	byte *c = (byte *) &y;
	c[0] = b[3];
	c[1] = b[2];
	c[2] = b[1];
	c[3] = b[0];
	return y;
}

uint32_t htonl(uint32_t x) {
	return ntohl(x);
}