//  Utility methods for protocol data type having a fixed length of 32 bit:
  
  //  Method to convert unsigned integers with bit length of 8,16,32 bit into uint_32
  template<typename uint>
  uint32_t uint_to_pdt_ufixed32(uint value){   
      uint32_t bit_mask_32_bit = 0;
      return bit_mask_32_bit | value;
  }

  uint32_t pdt_ufixed32_to_uint32(byte msg[], size_t itr){
    uint32_t value = 0;
    unsigned char *c = (unsigned char*)(&value);
    for(int i = 0;i < sizeof(uint32_t) ;i++){
      c[i] = msg[itr + i];
    }
    return value;
  }
  
  //  Method to convert signed integers (unsigned up tp 31 bit values) with bit length of 8,16,32 bit into int32_t
  template<typename sint>
  int32_t sint_to_pdt_sfixed32(sint value){   
      int32_t bit_mask_32_bit = 0;
      return bit_mask_32_bit + value;
  }

  int32_t pdt_sfixed32_to_sint32( byte msg[], size_t itr){   
    int32_t value = 0;
    unsigned char *c = (unsigned char*)(&value);
    for(int i = 0;i < sizeof(int32_t) ;i++){
      c[i] = msg[itr + i];
    }
    return value;
  }

  float pdt_float_to_float( byte msg[], size_t itr ){   
    float value = 0;
    unsigned char *c = ( unsigned char* )( &value );
    for( int i = 0; i < sizeof( float ) ; i++ ){
      c[ i ] = msg[ itr + i ];
    }
    return value;
  }

  
//  Utility methods for protocol data type having a fixed length of 64 bit:
  
  //  Method to convert unsigned integers with bit length of 8,16,32,64 bit into uint_64
  template<typename uint>
  uint64_t uint_to_pdt_ufixed64(uint value){   
      uint64_t bit_mask_64_bit = 0;
      return bit_mask_64_bit | value;
  }

  uint64_t pdt_ufixed64_to_uint64(byte msg[], size_t itr){
    uint64_t value = 0;
    unsigned char *c = (unsigned char*)(&value);
    for(int i = 0;i < sizeof(uint64_t) ;i++){
      c[i] = msg[itr + i];
    }
    return value;
  }
  
  //  Method to convert signed integers (unsigned up tp 31 bit values) with bit length of 8,16,32 bit into int32_t
  template<typename sint>
  int64_t sint_to_pdt_sfixed64(sint value){   
      int64_t bit_mask_64_bit = 0;
      return bit_mask_64_bit + value;
  }

  int64_t pdt_sfixed64_to_sint64( byte msg[], size_t itr){   
    int64_t value = 0;
    unsigned char *c = (unsigned char*)(&value);
    for(int i = 0;i < sizeof(int64_t) ;i++){
      c[i] = msg[itr + i];
    }
    return value;
  }

  // Method to copy each byte of a fixed length pdt, into a byte array with corresponding length(4/8 byte) in LE
  template<typename fixed_length_pdt>
  void fixed_size_pdt_to_byte_array(fixed_length_pdt value, byte byte_rep[]){
    byte *value_begin = (byte*)(&value);
    for(int i = 0; i < sizeof(value); i++){
      byte_rep[i] = value_begin[i];
    }
  }
  
//  Utility methods for protocol data types using Varing encoding:
  
  //  Method to convert unsigned ints(8,16,32,64 bit) to Varint. Returns the byte size of the varint.
  //  expects an unsigned int and a byte array of size 10(Max. encoding size for 64 bit).
  template<typename uint>
  size_t uint_to_varint(uint value, byte varint[]){
    size_t varint_size = 0;
    while(value>127){
        varint[varint_size] = ((((uint8_t) value) & 127) | 128);
        value >>=7;
        varint_size++;
    }
    varint[varint_size++] = ((uint8_t)value) & 127;
    return varint_size;
  }

  //  Method to convert signed ints(8,16,32,64 bit) to Varint. Uses zigzag encoding first.
  template<typename sint>
  size_t sint_to_varint(sint value, byte varint[]){
    uint64_t zigzag = sint_encode_zigzag(value);
    return uint_to_varint(zigzag, varint);
  }

  //  Method to calculate the byte size of an unsigned int in its varint encoding. Up tp 64 bit values.
  template<typename uint>
  size_t uint_varint_size(uint value){
    size_t varint_size = -1;
    if (value <= varint_one_byte_max_value)       varint_size = 1;
    else if (value <= varint_two_byte_max_value)  varint_size = 2;
    else if (value <= varint_three_byte_max_value)varint_size = 3;
    else if (value <= varint_four_byte_max_value) varint_size = 4;
    else if (value <= varint_five_byte_max_value) varint_size = 5;
    else if (value <= varint_six_byte_max_value)  varint_size = 6;
    else if (value <= varint_seven_byte_max_value)varint_size = 7;
    else if (value <= varint_eight_byte_max_value)varint_size = 8;
    else if (value <= varint_nine_byte_max_value) varint_size = 9;
    else if (value <= varint_64_bit_max_value)    varint_size = 10;
    return varint_size;
  }

  //  Method to calculate the byte size of a signed int in its varint encoding. Up to 63 (+ 1 sign bit) bit values.
  template<typename sint>
  size_t sint_varint_size(sint value){
    uint64_t zigzag = sint_encode_zigzag(value);
    return  uint_varint_size(sint_encode_zigzag(value));
  }

  //  Method to encode a signed integer (8,16,32,64 bit) with zigtag encoding
  //  Only use on signed integers.
  template<typename sint>
  uint64_t sint_encode_zigzag(sint value){
    uint8_t offset = (sizeof(value)*8) - 1; 
    return (value >> offset) ^ (value << 1);
  }

//  Utility methods for length delimited protocol data types:
  
  //  Encode text into the pdt:string
  void get_pdt_string(byte pdt_string[], byte text[], size_t text_byte_size){
    uint8_t varint[10];
    size_t varint_size = uint_to_varint(text_byte_size - 1, varint);
    size_t pdt_string_size = get_pdt_string_size(text_byte_size);
    
    for(int i = 0; i < varint_size; i++){
      pdt_string[i] = varint[i];
    }
    
    for(int j = varint_size; j < pdt_string_size; j++){
       pdt_string[j] = text[j - varint_size];
    }
  }
  
  //  Encode binary data into the pdt: bytes
  void get_pdt_bytes(byte pdt_bytes[], byte data[], size_t data_byte_size){
    uint8_t varint[10];
    size_t varint_size = uint_to_varint(data_byte_size, varint);
    size_t pdt_bytes_size = get_pdt_bytes_size(data_byte_size);
    
    for(int i = 0; i < varint_size; i++){
      pdt_bytes[i] = varint[i];
    }
    
    for(int j = varint_size; j < pdt_bytes_size; j++){
       pdt_bytes[j] = data[j - varint_size];
    }
  }
  
  //  Give pdt:string byte size without null termination!
  size_t get_pdt_string_size(size_t text_byte_size){
    return uint_varint_size(text_byte_size) + (text_byte_size - 1);
  }

  //  Give pdt: bytes size!
  size_t get_pdt_bytes_size(size_t data_byte_size){
    return uint_varint_size(data_byte_size) + (data_byte_size);
  }
