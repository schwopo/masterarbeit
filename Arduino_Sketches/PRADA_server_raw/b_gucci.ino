#include<Speck.h>
  //128 bit block size 
  //256 bit key size (NSA approved)

uint8_t gucci_shared_key[32] = {0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
                         0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
                         0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
                         0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x0a};

uint8_t gucci_session_key[32] = {0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
                         0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
                         0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
                         0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00};

void gucci_write_header(gucci_header header, byte *addr) {
  uint32_t size_n = htonl(header.size);
  uint32_t *size_addr = (uint32_t *) addr;
  *size_addr = size_n;

  addr[4] = header.padding;
}

byte gucci_msg_buffer[gucci_max_server_msg_size];

gucci_header gucci_read_header(byte *addr) {
  uint32_t size_n = ((uint32_t *) addr)[0];
  uint32_t size = ntohl(size_n);

  uint8_t padding = ((uint8_t *) addr)[4];

  gucci_header header;
  header.size = size;
  header.padding = padding;
  return header;
}

byte* gucci_make_msg(byte * msg, int msg_size, int *new_size) {
  //determine number of padding bytes
  int padding_size = get_padding_size(msg_size);

  //set up header
  gucci_header header;
  header.size = msg_size;
  header.padding = padding_size;

  //build  new msg
  byte *new_msg = gucci_msg_buffer;
  byte *content_addr = new_msg + gucci_header_size;
  byte *padding_addr = new_msg + gucci_header_size + msg_size;
  gucci_write_header(header, new_msg);
  memcpy(content_addr, msg, msg_size);
  memset(padding_addr, 0, padding_size);
  int complete_msg_size = gucci_header_size + msg_size + padding_size;
  *new_size = complete_msg_size;

  //encrypt message
  gucci_encrypt(new_msg, complete_msg_size, gucci_session_key);
  return new_msg;
}

int get_padding_size(int msg_size) {
  return gucci_block_size - ((gucci_header_size + msg_size) % gucci_block_size);
}

void gucci_encrypt(byte *msg, int msg_size, byte* gucci_key) {
  if(msg_size % gucci_block_size != 0)
    Serial.write("PADERRENC"); //padding error


  Speck *speck = new Speck();
  bool keysuccess = speck->setKey(gucci_key, 32);
  if(keysuccess == false) {
    // just don't encrypt bro ¯\_(ツ)_/¯
    return;
  }

  int n_blocks = msg_size / gucci_block_size;

  for(int i = 0; i < n_blocks; i++) {
    //Crypto lib can encrypt in place
    byte *block_addr = msg + (i * gucci_block_size);
    speck->encryptBlock(block_addr, block_addr);
  }
  delete(speck);
}

void gucci_decrypt(byte *msg, int msg_size, byte *gucci_key) {
  if(msg_size % gucci_block_size != 0)
    Serial.write("PADERRDEC"); //padding error


  Speck *speck = new Speck();
  bool keysuccess = speck->setKey(gucci_key, 32);
  if(keysuccess == false) {
    Serial.write("NODEC"); //padding error
    // just don't encrypt bro ¯\_(ツ)_/¯
    return;
  }

  int n_blocks = msg_size / gucci_block_size;

  for(int i = 0; i < n_blocks; i++) {
    //Crypto lib can encrypt in place
    byte *block_addr = msg + (i * gucci_block_size);
    speck->decryptBlock(block_addr, block_addr);
  }
  delete(speck);
}