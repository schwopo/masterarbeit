//SRV State Machine
                   //0               1               2               3               
//typedef enum {  SRV_STATE_WAITING, SRV_STATE_REC,  SRV_STATE_PROC, NUM_SRV_STATES } srv_state_t;
typedef srv_state_t srv_state_func_t();

srv_state_func_t* const srv_state_table[ NUM_SRV_STATES ] = {
    do_state_waiting, do_state_receiving, do_state_processing
};

srv_state_t run_srv_state( srv_state_t cur_srv_state ) {
    return srv_state_table[ cur_srv_state ]();
};

//UI State Machine
                  //0            1               2                3               4                   5
//typedef enum {  UI_STATE_INIT, UI_STATE_HOME,  UI_STATE_DINFOS, UI_STATE_TEMPS, UI_STATE_IMPRESSUM, NUM_UI_STATES } ui_state_t;
typedef ui_state_t ui_state_func_t(ui_data_t ui_data);

ui_state_func_t* const ui_state_table[ NUM_UI_STATES ] = {
    do_ui_state_noui
};

typedef size_t ui_transition_func_t(byte** msg_pointer);
ui_transition_func_t * const ui_transition_table[ NUM_UI_STATES ][ NUM_UI_STATES ] = {
    { NULL}
};

ui_state_t run_ui_state( ui_state_t cur_ui_state, ui_data_t data ) {
    return ui_state_table[cur_ui_state](data);
};
