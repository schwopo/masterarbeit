
void send_msg(byte* msg, size_t msg_size) {
  int new_size;
  byte *newmsg = gucci_make_msg(msg, msg_size, &new_size);
  send_switch_header(0, new_size);
  for ( int i = 0; i < new_size; ++i) {
    Serial.write(newmsg[i]);
  }
}
void send_msg_to_gateway(byte* msg, size_t msg_size) {
  int new_size;
  byte *newmsg = gucci_make_msg(msg, msg_size, &new_size);
  send_switch_header(1, new_size);
  for ( int i = 0; i < new_size; ++i) {
    Serial.write(newmsg[i]);
  }
}

void send_msg_plain(byte* msg, size_t msg_size) {
  send_switch_header(0, msg_size);
  for ( int i = 0; i < msg_size; ++i) {
    Serial.write(msg[i]);
  }
}

void send_msg_plain_to_gateway(byte* msg, size_t msg_size) {
  send_switch_header(1, msg_size);
  for ( int i = 0; i < msg_size; ++i) {
    Serial.write(msg[i]);
  }
}

void send_debug_msg(byte* msg, size_t msg_size) {
  send_switch_header(2, msg_size);
  for ( int i = 0; i < msg_size; ++i) {
    Serial.write(msg[i]);
  }
}

void send_debug_msg_ascii(byte* msg, size_t msg_size) {
  send_switch_header(3, msg_size);
  for ( int i = 0; i < msg_size; ++i) {
    Serial.write(msg[i]);
  }
}

void send_debug_msg_ascii(char *msg) {
  int length = strlen(msg);
  send_debug_msg_ascii(msg, length);
}

void send_switch_header(byte recipient, uint32_t size) {
  byte buffer[5];
  uint32_t *size_in_buffer = (uint32_t *) (buffer + 1);
  buffer[0] = recipient;
  *size_in_buffer = htonl(size);
  for ( int i = 0; i < 5; ++i) {
    Serial.write(buffer[i]);
  }
}

//  used for push notifications
void send_own_msg(byte* msg, size_t msg_size) {
  if (cur_con_state == CON_STATE_CONIDLE) {
    send_msg( msg, msg_size );
    msg_send_time = millis();
    last_own_msg = msg;
    last_own_msg_size = msg_size;
    cur_con_state = CON_STATE_CON_NEEDACK;
    sending_tries = 1;
  }
}

//  Used to reply to a client msg
void send_reply(byte* msg, size_t msg_size) {
  if (cur_con_state == CON_STATE_CONIDLE) {
    send_msg( msg, msg_size );
    last_reply = msg;
    //Reply has data and we need to check for timeout. This should always be the case atm. Else it is just Noop ACK.
    if ( ( msg[ 0 ] >> 4 ) != msg_id_noop ) {
      cur_con_state = CON_STATE_CON_NEEDACK;
      msg_send_time = millis();
      last_reply_size = msg_size;
      sending_tries = 1;
    }
  }
  //  We do need ACK for our own last message. This reply has to wait if it has data.
  // Our last msg can not have been a reply with data. A reply means the SRV had to wait itself for its answer.
  // So if we get data from the srv it means our last reply was received and ACKed with the last data message
  else if (cur_con_state == CON_STATE_CON_NEEDACK) {
    if ( ( msg[ 0 ] >> 4 ) == msg_id_noop ) {
      send_msg(msg, msg_size);
    } else {
      //TODO Put reply into queue, we send it after we have our ACK
    }
  }
}

//  Resend last msg with data, triggered by timeout so it can be reply or push msg.
//  Last_own_msg, last_reply can not both be 0 if i still need ACK.
//  Both cld be 1 if last reply was only ACK, but thats why we check for last_own_msg first
void resend_last_data_msg() {
  if ( last_own_msg_size > 0 ) {
    send_msg(last_own_msg, last_own_msg_size);
  } else if ( last_reply_size > 0 ) {
    send_msg(last_reply, last_reply_size);
  }
  msg_send_time = millis();
  sending_tries++;
}

// resend last reply, triggered if client seq no is the last one and not the next expected one.
void resend_last_reply() {
  if (last_reply_size > 0) {
    send_msg(last_reply, last_reply_size);

    //  If last reply is not Noop and the Client needs it again, our reply was not received and we still need ACK for it
    //  Reset the sending count, because Client is there.
    if (last_reply[ 0 ] >> 4 != 10 ) {
      msg_send_time = millis();
      sending_tries = 1;
    }
  }
}

void send_heartbeat(){
  byte *msg;
  size_t msg_size = heartbeat(&msg);
  send_own_msg( msg, msg_size );
}

void send_con_term() {
  byte *msg;
  byte *msg_parts[] = {con_term_msg};
  size_t part_sizes[] = {sizeof(con_term_msg)};
  size_t msg_size = build_byte_message(&msg,  msg_parts, part_sizes, 1, true, false, false );
  send_own_msg( msg, msg_size );
}

void ack_con_term(){
  byte *msg;
  size_t msg_size = noop_ack(&msg);
  send_msg( msg, msg_size );
  to_default();
}

void send_error() {
  byte *msg;
  byte *msg_parts[] = {error_msg};
  size_t part_sizes[] = {sizeof(error_msg)};
  int msg_size = build_byte_message(&msg,  msg_parts, part_sizes, 1, true, false, false );
  send_own_msg( msg, msg_size );
}

void send_error_no_con() { //For all non connected issues, that dont have the right seq/ack_no
  for (int i = 0; i < sizeof(no_con_error_msg); i++) {
    Serial.write(no_con_error_msg[i]);
  }
}
