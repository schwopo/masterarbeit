
//  -> SRV_STATE_REC
uint8_t msg_type      = 0;
uint8_t msg_size      = 0; //  stores how many bytes the message sld have
uint8_t payload_size  = 0;
uint8_t n_rec_bytes   = 0; //  how many bytes of the current client message were already received

//otway rees state
uint32_t or_own_id = 2;
int or_rb = 42069;
int or_awaiting_conreq = 1;
int or_awaiting_sesskey = 0;

byte received_msg[ max_client_msg_size ];
byte msg_buffer[gucci_max_msg_size]; 
byte *payload;

bool first_byte = true;
bool reading_payload = false;

bool authenticated = false;


srv_state_t do_state_receiving() {
  srv_state_t new_srv_state = cur_srv_state;

  if ( Serial.available() <= 0 ) 
    return new_srv_state;

  if (authenticated) {
    send_debug_msg_ascii("awaiting msg");
    new_srv_state = read_gucci_msg(cur_srv_state);
  }
  else
    read_or_msg();

  return new_srv_state;
}

srv_state_t read_gucci_msg(srv_state_t cur_srv_state) {
  srv_state_t new_srv_state = cur_srv_state;
  byte *msg = read_msg(msg_buffer, msg_buffer);

  while(new_srv_state == SRV_STATE_REC) {
    byte current_byte = *(msg++);
    new_srv_state = handle_prada_msg(current_byte, new_srv_state);
  }

  return new_srv_state;
}

int read_or_msg() {
  byte or_msg_id;
  Serial.readBytes(&or_msg_id, 1);

  //switch case doesn't work for some reason
  if(or_msg_id == or_connection_request) { 
      send_debug_msg_ascii("orcr", 4);
      int conreq_size = 28; //3 * 4 byte + 16 byte enc block
      Serial.readBytes(msg_buffer, conreq_size);
      uint32_t a;
      uint32_t b;
      uint32_t r;
      byte *enc_msg;
      read_connection_request(msg_buffer, &a, &b, &r, &enc_msg);

      if(b == or_own_id) {
        byte skreq[41];
        generate_session_key_request(skreq, a, b, r, or_rb, enc_msg);
        send_msg_plain_to_gateway(skreq, 41);
      }
  }

  else if(or_msg_id == or_session_key) {
      send_debug_msg_ascii("orsk", 4);
      int sk_msg_size = 3 * gucci_block_size;
      Serial.readBytes(msg_buffer, sk_msg_size);
      read_session_key(msg_buffer, gucci_session_key, or_rb);

      perform_ecdhe_exchange();

      authenticated = true;
  }

  else if(or_msg_id == coupling_static_pubk_request) {
    //contains gw public key in plaintext
    //respond with own static pubk, then generate shared secret and session key

    byte *other_public = msg_buffer;
    byte own_pk_msg[1 + ecdhe_public_key_size];
    Serial.readBytes(other_public, ecdhe_public_key_size);


    own_pk_msg[0]=coupling_static_pubk;
    memcpy(own_pk_msg+1, hard_public, ecdhe_public_key_size);

    int r = ecdhe_calculate_secret_on_hardcoded(other_public);
    sha256(gucci_session_key, ecdhe_secret, ecdhe_secret_size);

    send_msg_plain_to_gateway(own_pk_msg, 1+ecdhe_public_key_size);
  }

  else if(or_msg_id == coupling_ephemeral_pubk_request) {
      byte *other_public = msg_buffer;
      byte own_pk_msg[ecdhe_public_key_size + 1];

      Serial.readBytes(other_public, ecdhe_public_key_size);
      gucci_decrypt(other_public, 80, gucci_session_key);

      ecdhe_generate_key_pair();


      own_pk_msg[0]=coupling_ephemeral_pubk;
      ecdhe_generate_pk_message(own_pk_msg + 1);
      gucci_encrypt(own_pk_msg + 1, ecdhe_public_key_size, gucci_session_key);

      send_msg_plain_to_gateway(own_pk_msg, 1+ecdhe_public_key_size);

      int r = ecdhe_calculate_secret(other_public);
      or_own_id = ntohl(*(uint32_t *) (other_public + gucci_key_size));
      sha256(gucci_shared_key, ecdhe_secret, ecdhe_secret_size);
      write_shared_key();
      write_id();
  }

  else {
    send_debug_msg_ascii("unexpected code");
  }
}

void perform_ecdhe_exchange() {
      byte *other_pk_msg = msg_buffer; //64 + one block header
      byte other_public[ecdhe_public_key_size];
      byte own_pk_msg[ecdhe_public_key_msg_size];

      ecdhe_generate_key_pair();
      ecdhe_generate_pk_message(own_pk_msg);
      send_msg(own_pk_msg, ecdhe_public_key_msg_size);
      
      read_msg(other_pk_msg, other_public);

      int r = ecdhe_calculate_secret(other_public);

      sha256(gucci_session_key, ecdhe_secret, ecdhe_secret_size);
}

byte* read_msg(byte *buffer, byte *out_buffer){
  Serial.readBytes(buffer, gucci_block_size);
  gucci_decrypt(buffer, gucci_block_size, gucci_session_key);

  //read header
  gucci_header header = gucci_read_header(buffer);

  //read remaining msg
  int bytes_remaining = header.size + header.padding - (gucci_block_size - gucci_header_size);
  Serial.readBytes(buffer+gucci_block_size, bytes_remaining);
  gucci_decrypt(buffer+gucci_block_size, bytes_remaining, gucci_session_key);

  //check padding is zero
  //non-zero in padding means decryption error
  int padding_start = gucci_header_size + header.size;
  for (int i = 0; i < header.padding; i++) {
    if (buffer[padding_start + i] != 0) {
      send_debug_msg_ascii("encryption error");
      break;
    }
  }

  //copy into real msg buffer
   memcpy(out_buffer, buffer+gucci_header_size, header.size);

  return out_buffer;
}

srv_state_t handle_prada_msg(byte current_byte, srv_state_t par_srv_state) {
  srv_state_t new_srv_state = par_srv_state;
  if (reading_payload) {
    payload[n_rec_bytes - msg_size] = current_byte;
     n_rec_bytes++;
  } else {
    received_msg[ n_rec_bytes++ ] = current_byte;
  }

  if (first_byte) {
    msg_type = current_byte >> 4;
    msg_size = get_msg_size( msg_type, current_byte );
    first_byte = false;
  }

  if ( n_rec_bytes == ( msg_size + payload_size ) ) {
    if (msg_type == msg_id_user_input) {
      if (reading_payload) {
          new_srv_state = msg_read();
      } else {
        bool has_ack = bitRead( received_msg[ 0 ], 3 );
        uint8_t pdt_id = input_field_id_to_pdt[ received_msg[ has_ack ? 5 : 3  ]];

        if (pdt_id <= 63) {         // Fixed length 4 Byte
          payload_size = 4;
          payload = ( byte* )malloc( payload_size * sizeof( byte ) );
        } else if (pdt_id <= 127) { // Fixed length 8 Byte
          payload_size = 8;
          payload = ( byte* )malloc( payload_size * sizeof( byte ) );
        } else if(pdt_id <= 191) {  // varint
          
        }else{                      // length delimited
          
        }
        reading_payload = true;
      }
    } else {
        new_srv_state = msg_read();
    }
  }

  return new_srv_state;
}


srv_state_t msg_read() {
  reading_payload = false;
  first_byte = true;
  last_input_time = millis();
  return SRV_STATE_PROC;
}