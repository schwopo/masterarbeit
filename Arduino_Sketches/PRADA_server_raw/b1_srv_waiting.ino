//  -> SRV_STATE_WATING
unsigned long msg_send_time = 0;
uint8_t sending_tries = 0;

unsigned long con_term_time = 0;
unsigned long last_input_time = 0;

srv_state_t do_state_waiting() {
  srv_state_t new_srv_state = cur_srv_state;
  if ( Serial.available() > 0 ) {
    new_srv_state = SRV_STATE_REC;
  } else if (cur_con_state == CON_STATE_CON_NEEDACK) {
    //  Msg not acked for timeout time
    if ( millis() - msg_send_time >= msg_timeout_in_ms ) {
      if ( sending_tries >= max_tries ) {
        cur_con_state = CON_STATE_CONIDLE;
        send_con_term();
        con_term_time = millis();
        cur_con_state = CON_STATE_TERM;
      } else {
        resend_last_data_msg();
      }
    }
  } else if (cur_con_state == CON_STATE_CONIDLE) {
    if ( millis() - last_input_time >= heartbeat_in_ms ) {
      send_heartbeat();
    }
  }
  //  Timeout for Client answer after con_term_msg
  else if (cur_con_state == CON_STATE_TERM) {
    if ( millis() - con_term_time >=  term_timeout_in_ms ) {
      to_default();
    }
  }


  return new_srv_state;
}
