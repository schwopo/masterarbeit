//  ->  SRV_STATE_PROC
uint16_t server_sequence_no = 0;
uint16_t expected_client_sequence_no = 0;

byte * last_own_msg;
size_t last_own_msg_size = 0;
byte * last_reply;
size_t last_reply_size = 0;

srv_state_t do_state_processing() {
  byte* reply;
  size_t reply_size = 0;

  //Not yet connected
  if (cur_con_state == CON_STATE_NCON ) {
    if (msg_type == msg_id_con_init) {
      //  Check for supported protocol version:
      uint8_t client_protocol_version = received_msg[ 0 ] & 3;
      if (client_protocol_version !=  srv_protocol_version) {
        send_error_no_con();
      } else {
        expected_client_sequence_no++;

        if ( bitRead( received_msg[ 0 ], 3 ) ) {  // client wants ui?
        
        } else {
          reply_size = con_accept( &reply );
        }
        cur_con_state = CON_STATE_CONIDLE;
        send_reply( reply, reply_size );
      }
    } else {  //  Not connected. Another message than con_init does not make sense;
      send_error_no_con();
    }
  }

  //Connected and nothing that is still pending from my site
  else if ( cur_con_state == CON_STATE_CONIDLE ) {
    if ( msg_type != msg_id_con_init ) {

      uint16_t client_seq_no = 0;
      get_seq_no(&client_seq_no, received_msg);
      if (client_seq_no == expected_client_sequence_no) {
        expected_client_sequence_no++;
        free_last_reply();  //  In case it was only Noop ack
        bool msg_includes_ack = bitRead( received_msg[ 0 ], 3 );

        switch ( msg_type ) {
          case msg_id_link_clicked:
            {
              uint8_t link_id =  msg_includes_ack ? received_msg[5] : received_msg[3];
              ui_data_t link_clicked = { LINK_CLICKED, link_id, NULL };

              ui_state_t new_ui_state =  run_ui_state( cur_ui_state, link_clicked );

              ui_transition_func_t *ui_transition = ui_transition_table[ cur_ui_state ][ new_ui_state ];
              if ( ui_transition ) {
                reply_size = ui_transition( &reply );
              }

              cur_ui_state = new_ui_state;
              break;
            }

          case msg_id_user_input:
            {
              uint8_t pdt_id = input_field_id_to_pdt[ received_msg[ msg_includes_ack ? 5 : 3 ]];

              switch (pdt_id) {
                // Whatever has to be done here
              }

              if (payload_size > 0) {
                free(payload);
                payload_size = 0;
              }
              break;
            }

          case msg_id_con_term:
            ack_con_term();
            break;
        }

        if (reply_size > 0 ) {
          send_reply( reply, reply_size );
        }
      }

      // Client resends last message. Resend our answer to that. Can only be Noop ACK
      else if (client_seq_no == expected_client_sequence_no - 1) {
        // Maybe all our answers got lost. accept con_term.
        if ( msg_type == msg_id_con_term ) {
          ack_con_term();
        } else {
          resend_last_reply();
        }
      }
    }

    //  Already connected to a client.
    else {
      send_error_no_con();
    }
  }


  else if (cur_con_state == CON_STATE_CON_NEEDACK) {
    // extract ack_no if given:
    uint16_t ack_no = extract_ack_no();
    if (ack_no != 0) {
      if ( ack_no == server_sequence_no + 1 ) {
        got_ack();
      }
    } else {
      //  Msg has no ack. We are still waiting for our ack. Decide dependent on msg type what to do.
      //  If reply is noop ack, send it. If reply needs data, buffer reply and wait for our ack -> done in send_reply method.
      switch ( msg_type ) {
        case msg_id_con_init :
          //  We are already connected to some1.
          send_error_no_con();
          break;
        case msg_id_noop :
          // Should not be possible, there is always an ack in noop
          break;
        case msg_id_con_term:
          ack_con_term();
          break;
      }

      if (reply_size > 0 ) {
        send_reply( reply, reply_size );
      }
    }
  }


  else if (cur_con_state == CON_STATE_TERM) {
    // Look for ack:
    uint16_t ack_no = extract_ack_no();
    if (ack_no != 0) {
      if ( ack_no == server_sequence_no + 1 ) {
        to_default();
      }
    }
    else {
      send_error();
    }
  }

  msg_type = 0;
  msg_size = 0;
  n_rec_bytes = 0;

  return SRV_STATE_WAITING;
}

void got_ack() {
  cur_con_state =  CON_STATE_CONIDLE;
  if (last_own_msg_size > 0) {
    free(last_own_msg);
    last_own_msg_size = 0;
  }
  if (last_reply_size > 0) {
    free(last_reply);
    last_reply_size = 0;
  }

  sending_tries = 0;
  server_sequence_no++;
}

uint16_t extract_ack_no() {
  uint16_t ack_no = 0; //-> msg has no ack. Every msg from client except con init can have ack
  if ( msg_type != msg_id_con_init ) {
    get_ack_no( msg_type, &ack_no, received_msg );
  }
  return ack_no;
}

void free_last_reply() {
  if (last_reply_size > 0) {
    free(last_reply);
    last_reply_size = 0;
  }
}

void to_default() {
  if (last_own_msg_size > 0) {
    free(last_own_msg);
    last_own_msg_size = 0;
  }
  if (last_reply_size > 0) {
    free(last_reply);
    last_reply_size = 0;
  }
  sending_tries = 0;
  server_sequence_no = 0;
  expected_client_sequence_no = 0;
  cur_con_state = CON_STATE_NCON;

  cur_ui_state = UI_STATE_NOUI;

  con_term_time = 0;
}
