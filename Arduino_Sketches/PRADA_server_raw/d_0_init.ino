size_t con_accept(byte** msg_pointer){
  byte * messageParts[]       = {init_ds_pf}; //Default size is flaged 1 even if no UI is there to indicate the amount of protocol fields to the client.
  size_t noOfParts = sizeof(messageParts)/sizeof(messageParts[0]);    
  size_t messagePartsLength[] = {sizeof(init_ds_pf)};      
  //sendMessage(messageParts, messagePartsLength, noOfParts, true, true);
  return build_byte_message(msg_pointer, messageParts, messagePartsLength, noOfParts, true, true, true);
}
