Dieser CD ist der Code für den PRADA-GUCCI Prototypen beigefügt.
Er basiert auf dem PRADA-Code von Matthias Schaffeld.

Server enthält den Arduino-Server
-benötigte Bibliotheken befinden sich im Ordner Libraries
-er muss vor dem ersten Verbinden gekoppelt werden

Client enthält den Python-Client, Switch und Gateway
-er ist abhängig von den Bibliotheken ecdsa und simonspeckciphers (beide durch pip installierbar)
-Switch und Gateway müssen als eigenständige Programme laufen
-Arduino muss auf COM4 sein
-PyCharm wird empfohlen: Build Configurations für Verbindung und Kopplung sind enthalten

Missing Features:
-Authenticated Encryption
-Reliable Transport